# Kidney Glomeruli Segmentation UI App
This is a frontend app for the [kidney glomeruli segmentation app](https://gitlab.com/empaia/examples/kidney-glomeruli-segmentation-app).


## Setup UI app

To run tha app, it should be accessible over http. First, the app should be built and served.

```bash
docker build -t <ui-image-tag> .
docker run -p <port>:80 -it <ui-image-tag>
```

`nginx` is listening on port 80 (can be changed in `nginx.conf`) in the container and we map it to port `<port>` that will be used for registration. The app should be accessible at `http://localhost:<port>`

## Setup backend app

After this, the UI app has to be registered together with the backend app

```bash
git clone https://gitlab.com/empaia/examples/kidney-glomeruli-segmentation-app.git
```

Build docker

```bash
docker build -t <image-tag> ./
```

The `<image-tag> ` will be used for registration.


Please, see backend project's [README](https://gitlab.com/empaia/examples/kidney-glomeruli-segmentation-app) for its requirements.

## Register backend with UI

Now, the backend has to be registered using [eats](https://developer.empaia.org/app_developer_docs/v2/#/eats/eats) and linked to the UI app.

```bash
eats apps register --app-ui-url http://host.docker.internal:<port> --app-ui-config-file /path/to/frontend/app-ui-config-file.json path/to/backend/ead.json <image-tag> 
```

[--app-ui-url](https://developer.empaia.org/app_developer_docs/v2/#/eats/app_ui_url) associates the backend to the UI app and implicitly instructs the system to use WBS 2.0.

See `eats apps register --help` for more information.
