/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { AnnotationCountResponse } from '../models/annotation-count-response';
import { AnnotationList } from '../models/annotation-list';
import { AnnotationListResponse } from '../models/annotation-list-response';
import { AnnotationQuery } from '../models/annotation-query';
import { AnnotationQueryPosition } from '../models/annotation-query-position';
import { AnnotationUniqueClassesQuery } from '../models/annotation-unique-classes-query';
import { AnnotationViewerList } from '../models/annotation-viewer-list';
import { AnnotationViewerQuery } from '../models/annotation-viewer-query';
import { ArrowAnnotation } from '../models/arrow-annotation';
import { BoolPrimitive } from '../models/bool-primitive';
import { CircleAnnotation } from '../models/circle-annotation';
import { Class } from '../models/class';
import { ClassList } from '../models/class-list';
import { ClassListResponse } from '../models/class-list-response';
import { ClassQuery } from '../models/class-query';
import { ClassesDict } from '../models/classes-dict';
import { Collection } from '../models/collection';
import { CollectionList } from '../models/collection-list';
import { CollectionQuery } from '../models/collection-query';
import { FloatPrimitive } from '../models/float-primitive';
import { IdObject } from '../models/id-object';
import { IntegerPrimitive } from '../models/integer-primitive';
import { ItemQuery } from '../models/item-query';
import { ItemQueryList } from '../models/item-query-list';
import { LineAnnotation } from '../models/line-annotation';
import { Message } from '../models/message';
import { PointAnnotation } from '../models/point-annotation';
import { PolygonAnnotation } from '../models/polygon-annotation';
import { PostAnnotationList } from '../models/post-annotation-list';
import { PostArrowAnnotation } from '../models/post-arrow-annotation';
import { PostBoolPrimitive } from '../models/post-bool-primitive';
import { PostCircleAnnotation } from '../models/post-circle-annotation';
import { PostClass } from '../models/post-class';
import { PostClassList } from '../models/post-class-list';
import { PostCollection } from '../models/post-collection';
import { PostFloatPrimitive } from '../models/post-float-primitive';
import { PostIntegerPrimitive } from '../models/post-integer-primitive';
import { PostItemList } from '../models/post-item-list';
import { PostLineAnnotation } from '../models/post-line-annotation';
import { PostPointAnnotation } from '../models/post-point-annotation';
import { PostPolygonAnnotation } from '../models/post-polygon-annotation';
import { PostPrimitiveList } from '../models/post-primitive-list';
import { PostRectangleAnnotation } from '../models/post-rectangle-annotation';
import { PostStringPrimitive } from '../models/post-string-primitive';
import { PrimitiveList } from '../models/primitive-list';
import { PrimitiveQuery } from '../models/primitive-query';
import { RectangleAnnotation } from '../models/rectangle-annotation';
import { SlideItem } from '../models/slide-item';
import { StringPrimitive } from '../models/string-primitive';
import { UniqueClassValues } from '../models/unique-class-values';
import { UniqueReferences } from '../models/unique-references';
import { WorkbenchServiceApiV2ModelsAnnotationCollectionsSlideList } from '../models/workbench-service-api-v-2-models-annotation-collections-slide-list';

@Injectable({
  providedIn: 'root',
})
export class DataPanelService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation scopeIdAnnotationsQueryPut
   */
  static readonly ScopeIdAnnotationsQueryPutPath = '/{scope_id}/annotations/query';

  /**
   * Query annotations.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryPut$Response(params: {
    scope_id: string;
    with_classes?: boolean;
    with_low_npp_centroids?: boolean;
    skip?: number;
    limit?: number;
    body: AnnotationQuery
  }): Observable<StrictHttpResponse<AnnotationList>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdAnnotationsQueryPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.query('with_classes', params.with_classes, {});
      rb.query('with_low_npp_centroids', params.with_low_npp_centroids, {});
      rb.query('skip', params.skip, {});
      rb.query('limit', params.limit, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<AnnotationList>;
      })
    );
  }

  /**
   * Query annotations.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryPut(params: {
    scope_id: string;
    with_classes?: boolean;
    with_low_npp_centroids?: boolean;
    skip?: number;
    limit?: number;
    body: AnnotationQuery
  }): Observable<AnnotationList> {

    return this.scopeIdAnnotationsQueryPut$Response(params).pipe(
      map((r: StrictHttpResponse<AnnotationList>) => r.body as AnnotationList)
    );
  }

  /**
   * Path part for operation scopeIdAnnotationsQueryCountPut
   */
  static readonly ScopeIdAnnotationsQueryCountPutPath = '/{scope_id}/annotations/query/count';

  /**
   * Get count of queried annotation items.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsQueryCountPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryCountPut$Response(params: {
    scope_id: string;
    body: AnnotationQuery
  }): Observable<StrictHttpResponse<AnnotationCountResponse>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdAnnotationsQueryCountPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<AnnotationCountResponse>;
      })
    );
  }

  /**
   * Get count of queried annotation items.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsQueryCountPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryCountPut(params: {
    scope_id: string;
    body: AnnotationQuery
  }): Observable<AnnotationCountResponse> {

    return this.scopeIdAnnotationsQueryCountPut$Response(params).pipe(
      map((r: StrictHttpResponse<AnnotationCountResponse>) => r.body as AnnotationCountResponse)
    );
  }

  /**
   * Path part for operation scopeIdAnnotationsQueryUniqueClassValuesPut
   */
  static readonly ScopeIdAnnotationsQueryUniqueClassValuesPutPath = '/{scope_id}/annotations/query/unique-class-values';

  /**
   * Get unique class values of queried annotations.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsQueryUniqueClassValuesPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryUniqueClassValuesPut$Response(params: {
    scope_id: string;
    body: AnnotationUniqueClassesQuery
  }): Observable<StrictHttpResponse<UniqueClassValues>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdAnnotationsQueryUniqueClassValuesPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UniqueClassValues>;
      })
    );
  }

  /**
   * Get unique class values of queried annotations.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsQueryUniqueClassValuesPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryUniqueClassValuesPut(params: {
    scope_id: string;
    body: AnnotationUniqueClassesQuery
  }): Observable<UniqueClassValues> {

    return this.scopeIdAnnotationsQueryUniqueClassValuesPut$Response(params).pipe(
      map((r: StrictHttpResponse<UniqueClassValues>) => r.body as UniqueClassValues)
    );
  }

  /**
   * Path part for operation scopeIdAnnotationsQueryUniqueReferencesPut
   */
  static readonly ScopeIdAnnotationsQueryUniqueReferencesPutPath = '/{scope_id}/annotations/query/unique-references';

  /**
   * Get unique references of queried annotations.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsQueryUniqueReferencesPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryUniqueReferencesPut$Response(params: {
    scope_id: string;
    body: AnnotationQuery
  }): Observable<StrictHttpResponse<UniqueReferences>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdAnnotationsQueryUniqueReferencesPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UniqueReferences>;
      })
    );
  }

  /**
   * Get unique references of queried annotations.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsQueryUniqueReferencesPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryUniqueReferencesPut(params: {
    scope_id: string;
    body: AnnotationQuery
  }): Observable<UniqueReferences> {

    return this.scopeIdAnnotationsQueryUniqueReferencesPut$Response(params).pipe(
      map((r: StrictHttpResponse<UniqueReferences>) => r.body as UniqueReferences)
    );
  }

  /**
   * Path part for operation scopeIdAnnotationsQueryViewerPut
   */
  static readonly ScopeIdAnnotationsQueryViewerPutPath = '/{scope_id}/annotations/query/viewer';

  /**
   * Query annotations for viewing purposes.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsQueryViewerPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryViewerPut$Response(params: {
    scope_id: string;
    body: AnnotationViewerQuery
  }): Observable<StrictHttpResponse<AnnotationViewerList>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdAnnotationsQueryViewerPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<AnnotationViewerList>;
      })
    );
  }

  /**
   * Query annotations for viewing purposes.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsQueryViewerPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsQueryViewerPut(params: {
    scope_id: string;
    body: AnnotationViewerQuery
  }): Observable<AnnotationViewerList> {

    return this.scopeIdAnnotationsQueryViewerPut$Response(params).pipe(
      map((r: StrictHttpResponse<AnnotationViewerList>) => r.body as AnnotationViewerList)
    );
  }

  /**
   * Path part for operation scopeIdAnnotationsAnnotationIdQueryPut
   */
  static readonly ScopeIdAnnotationsAnnotationIdQueryPutPath = '/{scope_id}/annotations/{annotation_id}/query';

  /**
   * Get the postion of an annotation in the result query.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsAnnotationIdQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsAnnotationIdQueryPut$Response(params: {

    /**
     * Annotation ID
     */
    annotation_id: string;
    scope_id: string;
    body: AnnotationQuery
  }): Observable<StrictHttpResponse<AnnotationQueryPosition>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdAnnotationsAnnotationIdQueryPutPath, 'put');
    if (params) {
      rb.path('annotation_id', params.annotation_id, {});
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<AnnotationQueryPosition>;
      })
    );
  }

  /**
   * Get the postion of an annotation in the result query.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsAnnotationIdQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsAnnotationIdQueryPut(params: {

    /**
     * Annotation ID
     */
    annotation_id: string;
    scope_id: string;
    body: AnnotationQuery
  }): Observable<AnnotationQueryPosition> {

    return this.scopeIdAnnotationsAnnotationIdQueryPut$Response(params).pipe(
      map((r: StrictHttpResponse<AnnotationQueryPosition>) => r.body as AnnotationQueryPosition)
    );
  }

  /**
   * Path part for operation scopeIdAnnotationsPost
   */
  static readonly ScopeIdAnnotationsPostPath = '/{scope_id}/annotations';

  /**
   * Post annotations.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsPost$Response(params: {
    scope_id: string;
    is_roi?: boolean;
    external_ids?: boolean;
    body: (PostPointAnnotation | PostLineAnnotation | PostArrowAnnotation | PostCircleAnnotation | PostRectangleAnnotation | PostPolygonAnnotation | PostAnnotationList)
  }): Observable<StrictHttpResponse<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse)>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdAnnotationsPostPath, 'post');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.query('is_roi', params.is_roi, {});
      rb.query('external_ids', params.external_ids, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse)>;
      })
    );
  }

  /**
   * Post annotations.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAnnotationsPost(params: {
    scope_id: string;
    is_roi?: boolean;
    external_ids?: boolean;
    body: (PostPointAnnotation | PostLineAnnotation | PostArrowAnnotation | PostCircleAnnotation | PostRectangleAnnotation | PostPolygonAnnotation | PostAnnotationList)
  }): Observable<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse)> {

    return this.scopeIdAnnotationsPost$Response(params).pipe(
      map((r: StrictHttpResponse<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse)>) => r.body as (PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse))
    );
  }

  /**
   * Path part for operation scopeIdAnnotationsAnnotationIdGet
   */
  static readonly ScopeIdAnnotationsAnnotationIdGetPath = '/{scope_id}/annotations/{annotation_id}';

  /**
   * Get annotation by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsAnnotationIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAnnotationsAnnotationIdGet$Response(params: {

    /**
     * Class ID
     */
    annotation_id: string;
    scope_id: string;
    with_classes?: boolean;
  }): Observable<StrictHttpResponse<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation)>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdAnnotationsAnnotationIdGetPath, 'get');
    if (params) {
      rb.path('annotation_id', params.annotation_id, {});
      rb.path('scope_id', params.scope_id, {});
      rb.query('with_classes', params.with_classes, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation)>;
      })
    );
  }

  /**
   * Get annotation by ID.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsAnnotationIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAnnotationsAnnotationIdGet(params: {

    /**
     * Class ID
     */
    annotation_id: string;
    scope_id: string;
    with_classes?: boolean;
  }): Observable<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation)> {

    return this.scopeIdAnnotationsAnnotationIdGet$Response(params).pipe(
      map((r: StrictHttpResponse<(PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation)>) => r.body as (PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation))
    );
  }

  /**
   * Path part for operation scopeIdAnnotationsAnnotationIdDelete
   */
  static readonly ScopeIdAnnotationsAnnotationIdDeletePath = '/{scope_id}/annotations/{annotation_id}';

  /**
   * Delete annotation by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAnnotationsAnnotationIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAnnotationsAnnotationIdDelete$Response(params: {

    /**
     * Class ID
     */
    annotation_id: string;
    scope_id: string;
  }): Observable<StrictHttpResponse<IdObject>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdAnnotationsAnnotationIdDeletePath, 'delete');
    if (params) {
      rb.path('annotation_id', params.annotation_id, {});
      rb.path('scope_id', params.scope_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<IdObject>;
      })
    );
  }

  /**
   * Delete annotation by ID.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdAnnotationsAnnotationIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAnnotationsAnnotationIdDelete(params: {

    /**
     * Class ID
     */
    annotation_id: string;
    scope_id: string;
  }): Observable<IdObject> {

    return this.scopeIdAnnotationsAnnotationIdDelete$Response(params).pipe(
      map((r: StrictHttpResponse<IdObject>) => r.body as IdObject)
    );
  }

  /**
   * Path part for operation scopeIdClassesQueryPut
   */
  static readonly ScopeIdClassesQueryPutPath = '/{scope_id}/classes/query';

  /**
   * Query classes.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdClassesQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdClassesQueryPut$Response(params: {
    scope_id: string;
    with_unique_class_values?: boolean;
    skip?: number;
    limit?: number;
    body: ClassQuery
  }): Observable<StrictHttpResponse<ClassList>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdClassesQueryPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.query('with_unique_class_values', params.with_unique_class_values, {});
      rb.query('skip', params.skip, {});
      rb.query('limit', params.limit, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ClassList>;
      })
    );
  }

  /**
   * Query classes.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdClassesQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdClassesQueryPut(params: {
    scope_id: string;
    with_unique_class_values?: boolean;
    skip?: number;
    limit?: number;
    body: ClassQuery
  }): Observable<ClassList> {

    return this.scopeIdClassesQueryPut$Response(params).pipe(
      map((r: StrictHttpResponse<ClassList>) => r.body as ClassList)
    );
  }

  /**
   * Path part for operation scopeIdClassesQueryUniqueReferencesPut
   */
  static readonly ScopeIdClassesQueryUniqueReferencesPutPath = '/{scope_id}/classes/query/unique-references';

  /**
   * Get unique references of queried classes.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdClassesQueryUniqueReferencesPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdClassesQueryUniqueReferencesPut$Response(params: {
    scope_id: string;
    body: ClassQuery
  }): Observable<StrictHttpResponse<UniqueReferences>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdClassesQueryUniqueReferencesPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UniqueReferences>;
      })
    );
  }

  /**
   * Get unique references of queried classes.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdClassesQueryUniqueReferencesPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdClassesQueryUniqueReferencesPut(params: {
    scope_id: string;
    body: ClassQuery
  }): Observable<UniqueReferences> {

    return this.scopeIdClassesQueryUniqueReferencesPut$Response(params).pipe(
      map((r: StrictHttpResponse<UniqueReferences>) => r.body as UniqueReferences)
    );
  }

  /**
   * Path part for operation scopeIdClassesPost
   */
  static readonly ScopeIdClassesPostPath = '/{scope_id}/classes';

  /**
   * Post classes.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdClassesPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdClassesPost$Response(params: {
    scope_id: string;
    body: (PostClass | PostClassList)
  }): Observable<StrictHttpResponse<(Class | ClassListResponse)>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdClassesPostPath, 'post');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<(Class | ClassListResponse)>;
      })
    );
  }

  /**
   * Post classes.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdClassesPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdClassesPost(params: {
    scope_id: string;
    body: (PostClass | PostClassList)
  }): Observable<(Class | ClassListResponse)> {

    return this.scopeIdClassesPost$Response(params).pipe(
      map((r: StrictHttpResponse<(Class | ClassListResponse)>) => r.body as (Class | ClassListResponse))
    );
  }

  /**
   * Path part for operation scopeIdClassesClassIdGet
   */
  static readonly ScopeIdClassesClassIdGetPath = '/{scope_id}/classes/{class_id}';

  /**
   * Get class by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdClassesClassIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdClassesClassIdGet$Response(params: {

    /**
     * Class ID
     */
    class_id: string;
    scope_id: string;
  }): Observable<StrictHttpResponse<Class>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdClassesClassIdGetPath, 'get');
    if (params) {
      rb.path('class_id', params.class_id, {});
      rb.path('scope_id', params.scope_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Class>;
      })
    );
  }

  /**
   * Get class by ID.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdClassesClassIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdClassesClassIdGet(params: {

    /**
     * Class ID
     */
    class_id: string;
    scope_id: string;
  }): Observable<Class> {

    return this.scopeIdClassesClassIdGet$Response(params).pipe(
      map((r: StrictHttpResponse<Class>) => r.body as Class)
    );
  }

  /**
   * Path part for operation scopeIdClassesClassIdDelete
   */
  static readonly ScopeIdClassesClassIdDeletePath = '/{scope_id}/classes/{class_id}';

  /**
   * Delete class by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdClassesClassIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdClassesClassIdDelete$Response(params: {

    /**
     * Class ID
     */
    class_id: string;
    scope_id: string;
  }): Observable<StrictHttpResponse<IdObject>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdClassesClassIdDeletePath, 'delete');
    if (params) {
      rb.path('class_id', params.class_id, {});
      rb.path('scope_id', params.scope_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<IdObject>;
      })
    );
  }

  /**
   * Delete class by ID.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdClassesClassIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdClassesClassIdDelete(params: {

    /**
     * Class ID
     */
    class_id: string;
    scope_id: string;
  }): Observable<IdObject> {

    return this.scopeIdClassesClassIdDelete$Response(params).pipe(
      map((r: StrictHttpResponse<IdObject>) => r.body as IdObject)
    );
  }

  /**
   * Path part for operation scopeIdClassNamespacesGet
   */
  static readonly ScopeIdClassNamespacesGetPath = '/{scope_id}/class-namespaces';

  /**
   * Get class namespaces.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdClassNamespacesGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdClassNamespacesGet$Response(params: {
    scope_id: string;
  }): Observable<StrictHttpResponse<{
[key: string]: ClassesDict;
}>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdClassNamespacesGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{
        [key: string]: ClassesDict;
        }>;
      })
    );
  }

  /**
   * Get class namespaces.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdClassNamespacesGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdClassNamespacesGet(params: {
    scope_id: string;
  }): Observable<{
[key: string]: ClassesDict;
}> {

    return this.scopeIdClassNamespacesGet$Response(params).pipe(
      map((r: StrictHttpResponse<{
[key: string]: ClassesDict;
}>) => r.body as {
[key: string]: ClassesDict;
})
    );
  }

  /**
   * Path part for operation scopeIdCollectionsQueryPut
   */
  static readonly ScopeIdCollectionsQueryPutPath = '/{scope_id}/collections/query';

  /**
   * Query collections.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsQueryPut$Response(params: {
    scope_id: string;
    skip?: number;
    limit?: number;
    body: CollectionQuery
  }): Observable<StrictHttpResponse<CollectionList>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdCollectionsQueryPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.query('skip', params.skip, {});
      rb.query('limit', params.limit, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<CollectionList>;
      })
    );
  }

  /**
   * Query collections.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsQueryPut(params: {
    scope_id: string;
    skip?: number;
    limit?: number;
    body: CollectionQuery
  }): Observable<CollectionList> {

    return this.scopeIdCollectionsQueryPut$Response(params).pipe(
      map((r: StrictHttpResponse<CollectionList>) => r.body as CollectionList)
    );
  }

  /**
   * Path part for operation scopeIdCollectionsQueryUniqueReferencesPut
   */
  static readonly ScopeIdCollectionsQueryUniqueReferencesPutPath = '/{scope_id}/collections/query/unique-references';

  /**
   * Get unique references of queried collections.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsQueryUniqueReferencesPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsQueryUniqueReferencesPut$Response(params: {
    scope_id: string;
    body: CollectionQuery
  }): Observable<StrictHttpResponse<UniqueReferences>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdCollectionsQueryUniqueReferencesPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UniqueReferences>;
      })
    );
  }

  /**
   * Get unique references of queried collections.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsQueryUniqueReferencesPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsQueryUniqueReferencesPut(params: {
    scope_id: string;
    body: CollectionQuery
  }): Observable<UniqueReferences> {

    return this.scopeIdCollectionsQueryUniqueReferencesPut$Response(params).pipe(
      map((r: StrictHttpResponse<UniqueReferences>) => r.body as UniqueReferences)
    );
  }

  /**
   * Path part for operation scopeIdCollectionsCollectionIdItemsQueryPut
   */
  static readonly ScopeIdCollectionsCollectionIdItemsQueryPutPath = '/{scope_id}/collections/{collection_id}/items/query';

  /**
   * Query items of collection.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsCollectionIdItemsQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsCollectionIdItemsQueryPut$Response(params: {

    /**
     * Collection ID
     */
    collection_id: string;
    scope_id: string;
    skip?: number;
    limit?: number;
    body: ItemQuery
  }): Observable<StrictHttpResponse<ItemQueryList>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdCollectionsCollectionIdItemsQueryPutPath, 'put');
    if (params) {
      rb.path('collection_id', params.collection_id, {});
      rb.path('scope_id', params.scope_id, {});
      rb.query('skip', params.skip, {});
      rb.query('limit', params.limit, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ItemQueryList>;
      })
    );
  }

  /**
   * Query items of collection.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsCollectionIdItemsQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsCollectionIdItemsQueryPut(params: {

    /**
     * Collection ID
     */
    collection_id: string;
    scope_id: string;
    skip?: number;
    limit?: number;
    body: ItemQuery
  }): Observable<ItemQueryList> {

    return this.scopeIdCollectionsCollectionIdItemsQueryPut$Response(params).pipe(
      map((r: StrictHttpResponse<ItemQueryList>) => r.body as ItemQueryList)
    );
  }

  /**
   * Path part for operation scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut
   */
  static readonly ScopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPutPath = '/{scope_id}/collections/{collection_id}/items/query/unique-references';

  /**
   * Get unique references of queried items of a collection.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut$Response(params: {

    /**
     * Collection ID
     */
    collection_id: string;
    scope_id: string;
    body: ItemQuery
  }): Observable<StrictHttpResponse<UniqueReferences>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPutPath, 'put');
    if (params) {
      rb.path('collection_id', params.collection_id, {});
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UniqueReferences>;
      })
    );
  }

  /**
   * Get unique references of queried items of a collection.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut(params: {

    /**
     * Collection ID
     */
    collection_id: string;
    scope_id: string;
    body: ItemQuery
  }): Observable<UniqueReferences> {

    return this.scopeIdCollectionsCollectionIdItemsQueryUniqueReferencesPut$Response(params).pipe(
      map((r: StrictHttpResponse<UniqueReferences>) => r.body as UniqueReferences)
    );
  }

  /**
   * Path part for operation scopeIdCollectionsPost
   */
  static readonly ScopeIdCollectionsPostPath = '/{scope_id}/collections';

  /**
   * Post collection.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsPost$Response(params: {
    scope_id: string;
    body: PostCollection
  }): Observable<StrictHttpResponse<Collection>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdCollectionsPostPath, 'post');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Collection>;
      })
    );
  }

  /**
   * Post collection.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsPost(params: {
    scope_id: string;
    body: PostCollection
  }): Observable<Collection> {

    return this.scopeIdCollectionsPost$Response(params).pipe(
      map((r: StrictHttpResponse<Collection>) => r.body as Collection)
    );
  }

  /**
   * Path part for operation scopeIdCollectionsCollectionIdGet
   */
  static readonly ScopeIdCollectionsCollectionIdGetPath = '/{scope_id}/collections/{collection_id}';

  /**
   * Get collection by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsCollectionIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdCollectionsCollectionIdGet$Response(params: {

    /**
     * Collection ID
     */
    collection_id: string;
    scope_id: string;
  }): Observable<StrictHttpResponse<Collection>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdCollectionsCollectionIdGetPath, 'get');
    if (params) {
      rb.path('collection_id', params.collection_id, {});
      rb.path('scope_id', params.scope_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Collection>;
      })
    );
  }

  /**
   * Get collection by ID.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsCollectionIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdCollectionsCollectionIdGet(params: {

    /**
     * Collection ID
     */
    collection_id: string;
    scope_id: string;
  }): Observable<Collection> {

    return this.scopeIdCollectionsCollectionIdGet$Response(params).pipe(
      map((r: StrictHttpResponse<Collection>) => r.body as Collection)
    );
  }

  /**
   * Path part for operation scopeIdCollectionsCollectionIdDelete
   */
  static readonly ScopeIdCollectionsCollectionIdDeletePath = '/{scope_id}/collections/{collection_id}';

  /**
   * Delete collection by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsCollectionIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdCollectionsCollectionIdDelete$Response(params: {

    /**
     * Collection ID
     */
    collection_id: string;
    scope_id: string;
  }): Observable<StrictHttpResponse<IdObject>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdCollectionsCollectionIdDeletePath, 'delete');
    if (params) {
      rb.path('collection_id', params.collection_id, {});
      rb.path('scope_id', params.scope_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<IdObject>;
      })
    );
  }

  /**
   * Delete collection by ID.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsCollectionIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdCollectionsCollectionIdDelete(params: {

    /**
     * Collection ID
     */
    collection_id: string;
    scope_id: string;
  }): Observable<IdObject> {

    return this.scopeIdCollectionsCollectionIdDelete$Response(params).pipe(
      map((r: StrictHttpResponse<IdObject>) => r.body as IdObject)
    );
  }

  /**
   * Path part for operation scopeIdCollectionsCollectionIdItemsPost
   */
  static readonly ScopeIdCollectionsCollectionIdItemsPostPath = '/{scope_id}/collections/{collection_id}/items';

  /**
   * Get collection item to existing collection.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsCollectionIdItemsPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsCollectionIdItemsPost$Response(params: {

    /**
     * Collection ID
     */
    collection_id: string;
    scope_id: string;
    body: (PostPointAnnotation | PostLineAnnotation | PostArrowAnnotation | PostCircleAnnotation | PostRectangleAnnotation | PostPolygonAnnotation | PostClass | PostIntegerPrimitive | PostFloatPrimitive | PostBoolPrimitive | PostStringPrimitive | SlideItem | IdObject | PostCollection | PostItemList)
  }): Observable<StrictHttpResponse<(Message | PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse | IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList | Class | ClassListResponse | SlideItem | WorkbenchServiceApiV2ModelsAnnotationCollectionsSlideList | Collection | CollectionList)>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdCollectionsCollectionIdItemsPostPath, 'post');
    if (params) {
      rb.path('collection_id', params.collection_id, {});
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<(Message | PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse | IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList | Class | ClassListResponse | SlideItem | WorkbenchServiceApiV2ModelsAnnotationCollectionsSlideList | Collection | CollectionList)>;
      })
    );
  }

  /**
   * Get collection item to existing collection.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsCollectionIdItemsPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdCollectionsCollectionIdItemsPost(params: {

    /**
     * Collection ID
     */
    collection_id: string;
    scope_id: string;
    body: (PostPointAnnotation | PostLineAnnotation | PostArrowAnnotation | PostCircleAnnotation | PostRectangleAnnotation | PostPolygonAnnotation | PostClass | PostIntegerPrimitive | PostFloatPrimitive | PostBoolPrimitive | PostStringPrimitive | SlideItem | IdObject | PostCollection | PostItemList)
  }): Observable<(Message | PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse | IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList | Class | ClassListResponse | SlideItem | WorkbenchServiceApiV2ModelsAnnotationCollectionsSlideList | Collection | CollectionList)> {

    return this.scopeIdCollectionsCollectionIdItemsPost$Response(params).pipe(
      map((r: StrictHttpResponse<(Message | PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse | IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList | Class | ClassListResponse | SlideItem | WorkbenchServiceApiV2ModelsAnnotationCollectionsSlideList | Collection | CollectionList)>) => r.body as (Message | PointAnnotation | LineAnnotation | ArrowAnnotation | CircleAnnotation | RectangleAnnotation | PolygonAnnotation | AnnotationListResponse | IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList | Class | ClassListResponse | SlideItem | WorkbenchServiceApiV2ModelsAnnotationCollectionsSlideList | Collection | CollectionList))
    );
  }

  /**
   * Path part for operation scopeIdCollectionsCollectionIdItemsItemIdDelete
   */
  static readonly ScopeIdCollectionsCollectionIdItemsItemIdDeletePath = '/{scope_id}/collections/{collection_id}/items/{item_id}';

  /**
   * Delete collection item from existing collection.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdCollectionsCollectionIdItemsItemIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdCollectionsCollectionIdItemsItemIdDelete$Response(params: {

    /**
     * Collection ID
     */
    collection_id: string;

    /**
     * Collection Item ID
     */
    item_id: string;
    scope_id: string;
  }): Observable<StrictHttpResponse<IdObject>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdCollectionsCollectionIdItemsItemIdDeletePath, 'delete');
    if (params) {
      rb.path('collection_id', params.collection_id, {});
      rb.path('item_id', params.item_id, {});
      rb.path('scope_id', params.scope_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<IdObject>;
      })
    );
  }

  /**
   * Delete collection item from existing collection.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdCollectionsCollectionIdItemsItemIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdCollectionsCollectionIdItemsItemIdDelete(params: {

    /**
     * Collection ID
     */
    collection_id: string;

    /**
     * Collection Item ID
     */
    item_id: string;
    scope_id: string;
  }): Observable<IdObject> {

    return this.scopeIdCollectionsCollectionIdItemsItemIdDelete$Response(params).pipe(
      map((r: StrictHttpResponse<IdObject>) => r.body as IdObject)
    );
  }

  /**
   * Path part for operation scopeIdPrimitivesQueryPut
   */
  static readonly ScopeIdPrimitivesQueryPutPath = '/{scope_id}/primitives/query';

  /**
   * Query primitives.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPrimitivesQueryPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPrimitivesQueryPut$Response(params: {
    scope_id: string;
    skip?: number;
    limit?: number;
    body: PrimitiveQuery
  }): Observable<StrictHttpResponse<PrimitiveList>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdPrimitivesQueryPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.query('skip', params.skip, {});
      rb.query('limit', params.limit, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<PrimitiveList>;
      })
    );
  }

  /**
   * Query primitives.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdPrimitivesQueryPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPrimitivesQueryPut(params: {
    scope_id: string;
    skip?: number;
    limit?: number;
    body: PrimitiveQuery
  }): Observable<PrimitiveList> {

    return this.scopeIdPrimitivesQueryPut$Response(params).pipe(
      map((r: StrictHttpResponse<PrimitiveList>) => r.body as PrimitiveList)
    );
  }

  /**
   * Path part for operation scopeIdPrimitivesQueryUniqueReferencesPut
   */
  static readonly ScopeIdPrimitivesQueryUniqueReferencesPutPath = '/{scope_id}/primitives/query/unique-references';

  /**
   * Get unique references of queried primitives.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPrimitivesQueryUniqueReferencesPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPrimitivesQueryUniqueReferencesPut$Response(params: {
    scope_id: string;
    body: PrimitiveQuery
  }): Observable<StrictHttpResponse<UniqueReferences>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdPrimitivesQueryUniqueReferencesPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<UniqueReferences>;
      })
    );
  }

  /**
   * Get unique references of queried primitives.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdPrimitivesQueryUniqueReferencesPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPrimitivesQueryUniqueReferencesPut(params: {
    scope_id: string;
    body: PrimitiveQuery
  }): Observable<UniqueReferences> {

    return this.scopeIdPrimitivesQueryUniqueReferencesPut$Response(params).pipe(
      map((r: StrictHttpResponse<UniqueReferences>) => r.body as UniqueReferences)
    );
  }

  /**
   * Path part for operation scopeIdPrimitivesPost
   */
  static readonly ScopeIdPrimitivesPostPath = '/{scope_id}/primitives';

  /**
   * Post primitives.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPrimitivesPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPrimitivesPost$Response(params: {
    scope_id: string;
    body: (PostIntegerPrimitive | PostFloatPrimitive | PostBoolPrimitive | PostStringPrimitive | PostPrimitiveList)
  }): Observable<StrictHttpResponse<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList)>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdPrimitivesPostPath, 'post');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList)>;
      })
    );
  }

  /**
   * Post primitives.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdPrimitivesPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdPrimitivesPost(params: {
    scope_id: string;
    body: (PostIntegerPrimitive | PostFloatPrimitive | PostBoolPrimitive | PostStringPrimitive | PostPrimitiveList)
  }): Observable<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList)> {

    return this.scopeIdPrimitivesPost$Response(params).pipe(
      map((r: StrictHttpResponse<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList)>) => r.body as (IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | PrimitiveList))
    );
  }

  /**
   * Path part for operation scopeIdPrimitivesPrimitiveIdGet
   */
  static readonly ScopeIdPrimitivesPrimitiveIdGetPath = '/{scope_id}/primitives/{primitive_id}';

  /**
   * Get primitive by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPrimitivesPrimitiveIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPrimitivesPrimitiveIdGet$Response(params: {

    /**
     * Primitive ID
     */
    primitive_id: string;
    scope_id: string;
  }): Observable<StrictHttpResponse<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive)>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdPrimitivesPrimitiveIdGetPath, 'get');
    if (params) {
      rb.path('primitive_id', params.primitive_id, {});
      rb.path('scope_id', params.scope_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive)>;
      })
    );
  }

  /**
   * Get primitive by ID.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdPrimitivesPrimitiveIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPrimitivesPrimitiveIdGet(params: {

    /**
     * Primitive ID
     */
    primitive_id: string;
    scope_id: string;
  }): Observable<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive)> {

    return this.scopeIdPrimitivesPrimitiveIdGet$Response(params).pipe(
      map((r: StrictHttpResponse<(IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive)>) => r.body as (IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive))
    );
  }

  /**
   * Path part for operation scopeIdPrimitivesPrimitiveIdDelete
   */
  static readonly ScopeIdPrimitivesPrimitiveIdDeletePath = '/{scope_id}/primitives/{primitive_id}';

  /**
   * Delete primitive by ID.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdPrimitivesPrimitiveIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPrimitivesPrimitiveIdDelete$Response(params: {

    /**
     * Primitive ID
     */
    primitive_id: string;
    scope_id: string;
  }): Observable<StrictHttpResponse<IdObject>> {

    const rb = new RequestBuilder(this.rootUrl, DataPanelService.ScopeIdPrimitivesPrimitiveIdDeletePath, 'delete');
    if (params) {
      rb.path('primitive_id', params.primitive_id, {});
      rb.path('scope_id', params.scope_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<IdObject>;
      })
    );
  }

  /**
   * Delete primitive by ID.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdPrimitivesPrimitiveIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdPrimitivesPrimitiveIdDelete(params: {

    /**
     * Primitive ID
     */
    primitive_id: string;
    scope_id: string;
  }): Observable<IdObject> {

    return this.scopeIdPrimitivesPrimitiveIdDelete$Response(params).pipe(
      map((r: StrictHttpResponse<IdObject>) => r.body as IdObject)
    );
  }

}
