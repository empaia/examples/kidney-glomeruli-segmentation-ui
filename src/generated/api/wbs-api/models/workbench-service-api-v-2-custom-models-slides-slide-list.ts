/* tslint:disable */
/* eslint-disable */
import { Slide } from './slide';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface WorkbenchServiceApiV2CustomModelsSlidesSlideList {

  /**
   * Number of slides (not affected by skip/limit pagination)
   */
  item_count: number;

  /**
   * List of slides (affected by skip/limit pagination)
   */
  items: Array<Slide>;
}
