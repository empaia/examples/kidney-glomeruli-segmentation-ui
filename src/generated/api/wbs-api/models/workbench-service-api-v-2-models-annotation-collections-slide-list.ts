/* tslint:disable */
/* eslint-disable */
import { SlideItem } from './slide-item';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface WorkbenchServiceApiV2ModelsAnnotationCollectionsSlideList {

  /**
   * Count of all items
   */
  item_count: number;

  /**
   * List of items
   */
  items: Array<SlideItem>;
}
