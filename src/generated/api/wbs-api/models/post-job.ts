/* tslint:disable */
/* eslint-disable */
import { JobCreatorType } from './job-creator-type';

/**
 * This is sent by the workbench-service to the job-service to request the creation of a new Job.
 * The full EAD has to be submitted before the Job is created; Job-Inputs are added after creation.
 */
export interface PostJob {

  /**
   * The ID of the app to start, including the exact version of the app
   */
  app_id: string;

  /**
   * ID of the scope or user, that created the job
   */
  creator_id: string;

  /**
   * The type of creator that created the job. This can be a scope or a user (only for WBS v1)
   */
  creator_type: JobCreatorType;
}
