/* tslint:disable */
/* eslint-disable */
import { Collection } from './collection';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface CollectionList {

  /**
   * Count of items.
   */
  item_count: number;

  /**
   * List of items.
   */
  items: Array<Collection>;
}
