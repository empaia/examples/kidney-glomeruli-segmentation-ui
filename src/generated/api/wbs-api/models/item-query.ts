/* tslint:disable */
/* eslint-disable */
import { Viewport } from './viewport';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface ItemQuery {

  /**
   * Resolution range in npp (nanometer per pixel) to filter annotations (only used for annotations)
   */
  npp_viewing?: Array<number>;

  /**
   * List of reference Ids
   */
  references?: Array<string>;

  /**
   * The viewport (only used for annotations: only annotations within are returned)
   */
  viewport?: Viewport;
}
