/* tslint:disable */
/* eslint-disable */

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface AnnotationCountResponse {

  /**
   * Count of all items
   */
  item_count: number;
}
