/* tslint:disable */
/* eslint-disable */
import { SlideColor } from './slide-color';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface SlideChannel {

  /**
   * RGBA-value of the image channel
   */
  color: SlideColor;

  /**
   * Channel ID
   */
  id: number;

  /**
   * Dedicated channel name
   */
  name: string;
}
