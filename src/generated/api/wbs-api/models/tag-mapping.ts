/* tslint:disable */
/* eslint-disable */

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface TagMapping {
  mappings: {
[key: string]: string;
};
  tag: string;
}
