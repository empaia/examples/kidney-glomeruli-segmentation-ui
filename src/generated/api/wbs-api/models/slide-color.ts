/* tslint:disable */
/* eslint-disable */

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface SlideColor {

  /**
   * A-value (alpha channel)
   */
  'a': number;

  /**
   * B-value (blue channel)
   */
  'b': number;

  /**
   * G-value (green channel)
   */
  'g': number;

  /**
   * R-value (red channel)
   */
  'r': number;
}
