/* tslint:disable */
/* eslint-disable */

/**
 * An enumeration.
 */
export enum PrimitiveType {
  Integer = 'integer',
  Float = 'float',
  Bool = 'bool',
  String = 'string'
}
