/* tslint:disable */
/* eslint-disable */

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface ExtendedScope {

  /**
   * App ID
   */
  app_id: string;

  /**
   * UNIX timestamp in seconds - set by server
   */
  created_at: number;

  /**
   * EMPAIA app description
   */
  ead: {
};

  /**
   * Examination ID
   */
  examination_id: string;

  /**
   * Scope ID
   */
  id: string;

  /**
   * User ID
   */
  user_id: string;
}
