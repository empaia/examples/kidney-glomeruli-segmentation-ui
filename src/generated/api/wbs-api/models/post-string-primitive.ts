/* tslint:disable */
/* eslint-disable */
import { DataCreatorType } from './data-creator-type';
import { PrimitiveReferenceType } from './primitive-reference-type';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface PostStringPrimitive {

  /**
   * Id of the creator of this primitive
   */
  creator_id: string;

  /**
   * Creator type
   */
  creator_type: DataCreatorType;

  /**
   * Primitive description
   */
  description?: string;

  /**
   * ID of type UUID4 (only needed in post if external Ids enabled)
   */
  id?: string;

  /**
   * Primitive name
   */
  name: string;

  /**
   * Id of the object referenced by this primitive
   */
  reference_id?: string;

  /**
   * Reference type
   */
  reference_type?: PrimitiveReferenceType;

  /**
   * String type
   */
  type: 'string';

  /**
   * String value
   */
  value: string;
}
