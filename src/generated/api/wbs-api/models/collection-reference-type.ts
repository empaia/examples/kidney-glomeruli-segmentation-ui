/* tslint:disable */
/* eslint-disable */

/**
 * An enumeration.
 */
export enum CollectionReferenceType {
  Annotation = 'annotation',
  Wsi = 'wsi'
}
