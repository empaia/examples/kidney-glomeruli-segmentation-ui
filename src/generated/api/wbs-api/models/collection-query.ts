/* tslint:disable */
/* eslint-disable */
import { CollectionItemType } from './collection-item-type';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface CollectionQuery {

  /**
   * List of creator Ids
   */
  creators?: Array<string>;

  /**
   * List of item types
   */
  item_types?: Array<CollectionItemType>;

  /**
   * List of job Ids
   */
  jobs?: Array<string>;

  /**
   * List of reference Ids
   */
  references?: Array<string>;
}
