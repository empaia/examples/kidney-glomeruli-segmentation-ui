/* tslint:disable */
/* eslint-disable */
import { SlideExtent } from './slide-extent';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface SlideLevel {

  /**
   * Downsample factor for this level
   */
  downsample_factor: number;

  /**
   * Image extent for this level
   */
  extent: SlideExtent;
}
