/* tslint:disable */
/* eslint-disable */

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface UniqueReferences {

  /**
   * List of unique referenced annotation IDs (type UUID4)
   */
  annotation?: Array<string>;

  /**
   * List of unique referenced collection IDs (type UUID4)
   */
  collection?: Array<string>;

  /**
   * If true: there are items matching the filter criteria without a reference
   */
  contains_items_without_reference?: boolean;

  /**
   * List of unique referenced WSI IDs (type string)
   */
  wsi?: Array<string>;
}
