/* tslint:disable */
/* eslint-disable */
import { AnnotationReferenceType } from './annotation-reference-type';
import { Class } from './class';
import { DataCreatorType } from './data-creator-type';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface RectangleAnnotation {

  /**
   * Centroid of the annotation
   */
  centroid?: Array<number>;

  /**
   * List of classes assigned to annotation (if with_classes is true)
   */
  classes?: Array<Class>;

  /**
   * UNIX timestamp in seconds - set by server
   */
  created_at?: number;

  /**
   * Creator ID
   */
  creator_id: string;

  /**
   * Creator type
   */
  creator_type: DataCreatorType;

  /**
   * Annotation description
   */
  description?: string;

  /**
   * Rectangle height (must be > 0)
   */
  height: number;

  /**
   * ID of type UUID4 (only needed in post if external Ids enabled)
   */
  id?: string;

  /**
   * Flag to mark an annotation as immutable
   */
  is_locked?: boolean;

  /**
   * Annotation name
   */
  name: string;

  /**
   * Resolution in npp (nanometer per pixel) used to indicate on which layer the annotation is created
   */
  npp_created: number;

  /**
   * Recommended viewing resolution range in npp (nanometer per pixel) - Can be set by app
   */
  npp_viewing?: Array<number>;

  /**
   * ID of referenced Slide
   */
  reference_id: string;

  /**
   * Reference type (must be "wsi")
   */
  reference_type: AnnotationReferenceType;

  /**
   * Rectangle annotation
   */
  type: 'rectangle';

  /**
   * UNIX timestamp in seconds - set by server
   */
  updated_at?: number;

  /**
   * Point coordinates of upper left corner of the rectangle (must be >= 0)
   */
  upper_left: Array<number>;

  /**
   * Rectangle width (must be > 0)
   */
  width: number;
}
