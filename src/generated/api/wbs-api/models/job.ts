/* tslint:disable */
/* eslint-disable */
import { JobCreatorType } from './job-creator-type';
import { JobStatus } from './job-status';

/**
 * This describes the actual job and is a superset of the job-request, adding status parameters that are added
 * after the job has been created, such as the access-token, but also status-information and references to the created
 * output.
 */
export interface Job {

  /**
   * The ID of the app to start, including the exact version of the app
   */
  app_id: string;

  /**
   * Time when the job was created
   */
  created_at: number;

  /**
   * ID of the scope or user, that created the job
   */
  creator_id: string;

  /**
   * The type of creator that created the job. This can be a scope or a user (only for WBS v1)
   */
  creator_type: JobCreatorType;

  /**
   * The full EAD description of the app; this field is not stored in the database as part of the job, but separately and can be added to the job when fetching a job by its Id
   */
  ead?: {
};

  /**
   * Time when the job was completed or when it failed
   */
  ended_at?: number;

  /**
   * Optional error message in case the job failed
   */
  error_message?: string;

  /**
   * The unique ID of the job, set by the database
   */
  id: string;

  /**
   * Data references to input parameters, added after job creation
   */
  inputs: {
[key: string]: string;
};

  /**
   * Data references to output values, added when the job is being executed
   */
  outputs: {
[key: string]: string;
};

  /**
   * Time in seconds the job is running (if status RUNNING) or was running (if status COMPLETED)
   */
  runtime?: number;

  /**
   * Time when execution of the job was started
   */
  started_at?: number;

  /**
   * The current status of the job
   */
  status: JobStatus;
}
