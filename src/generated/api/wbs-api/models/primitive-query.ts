/* tslint:disable */
/* eslint-disable */
import { PrimitiveType } from './primitive-type';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface PrimitiveQuery {

  /**
   * List of creator Ids
   */
  creators?: Array<string>;

  /**
   * List of job Ids
   */
  jobs?: Array<string>;

  /**
   * List of reference Ids. IMPORTANT NOTE: Can be null, if primitives without reference should be included!
   */
  references?: Array<string>;

  /**
   * List of primitive types
   */
  types?: Array<PrimitiveType>;
}
