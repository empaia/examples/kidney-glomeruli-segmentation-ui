export { ScopePanelService } from './services/scope-panel.service';
export { JobsPanelService } from './services/jobs-panel.service';
export { SlidesPanelService } from './services/slides-panel.service';
export { DataPanelService } from './services/data-panel.service';
