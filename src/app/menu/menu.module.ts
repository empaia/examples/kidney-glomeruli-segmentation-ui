import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { MENU_MODULE_FEATURE_KEY, reducers } from '@menu/store/menu-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { ResultsModule } from '@results/results.module';
import { SlidesModule } from '@slides/slides.module';
import { MenuEffects } from '@menu/store';
import { MaterialModule } from '@material/material.module';
import { MenuComponent } from './containers/menu/menu.component';
import { SharedModule } from '@shared/shared.module';
import { ReactiveComponentModule } from '@ngrx/component';



@NgModule({
  declarations: [
    MenuComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ResultsModule,
    SharedModule,
    SlidesModule,
    StoreModule.forFeature(
      MENU_MODULE_FEATURE_KEY,
      reducers,
    ),
    EffectsModule.forFeature([
      MenuEffects,
    ]),
    SlidesModule,
    ReactiveComponentModule,
  ],
  exports: [
    MenuComponent
  ]
})
export class MenuModule { }
