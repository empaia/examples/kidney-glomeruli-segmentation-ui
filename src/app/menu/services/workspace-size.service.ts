import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WorkspaceSizeService {
  /**
   * Workspace is the middle column between button and open menus
   * overview & toolbar are positioned in this workspace area
   */

  private renderer: Renderer2;

  constructor(private rf: RendererFactory2) {
    this.renderer = rf.createRenderer(null, null);
  }

  setSlideViewerToolbarPosition(domRect: DOMRect) {
    const elTb = document.body.querySelector('.svm-toolbar');
    if (elTb) {
      this.renderer.setStyle(elTb, 'left', domRect.x + 'px');
    }
  }

  setSlideViewerOverviewPosition(domRect: DOMRect) {
    // we only want to update the overview window position if the user has not moved the window
    const elOv = document.body.querySelector('.svm-overview');
    // get slide viewer drag n drop element
    const elDnd = document.body.querySelector('.svm-overview-drag-box');
    const elBody = document.body;
    // logger.log('drag-box', elDnd);
    if (elDnd && elBody && elOv) {
      const transform = getComputedStyle(elDnd).transform;
      const position = getComputedStyle(elDnd).position;

      // logger.log('drag-box position', position);
      // logger.log('drag-box transform', transform);

      const bodyRect = elBody.getBoundingClientRect();
      // logger.log('[WorkspaceSizeService] body', bodyRect);
      // logger.log('[WorkspaceSizeService] set Overview El', domRect);

      /* if drag n drop box has a transform css property it was moved by the user
       * in this case more advanced calculations would be needed to postion the
       * overview correctly
       * for now we don't want to move the overview if it was moved by the user
       */
      if (transform === 'none') {
        // if pos is 'fixed' user has changed size of overview
        if (position === 'fixed') {
          this.renderer.removeStyle(elDnd, 'left');
          const pos = bodyRect.width - domRect.right;
          this.renderer.setStyle(elDnd, 'right', pos + 'px');
          // this.renderer.setStyle(elDnd, 'right', domRect.x + 'px');
        } else if (position === 'absolute') {
          const pos = bodyRect.width - domRect.right;
          this.renderer.setStyle(elOv, 'right', pos + 'px');
        }
      }
    }
  }
}
