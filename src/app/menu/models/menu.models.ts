/**
 * Interface of the menu data
 */
export type MenuType =
  | 'slides'
  | 'results';

export enum MenuState {
  HIDDEN,
  VISIBLE
}

export const MenuIcons: Record<MenuType, string> = {
  slides: 'filter',
  results: 'insights',
};

export interface MenuEntity {
  readonly id: MenuType;
  selected: boolean;
  contentState: MenuState;
  labelState: MenuState;
  readonly alignment: 'start' | 'end'; // start = left, end = right
  readonly order: number, // sort buttons low to high (1 = top most button)
  readonly icon: string,
}

// Initial Menu state definition
export const MENU: Record<MenuType, MenuEntity> = {
  slides: {
    id: 'slides',
    contentState: MenuState.HIDDEN,
    labelState: MenuState.VISIBLE,
    selected: false,
    alignment: 'end',
    order: 1,
    icon: MenuIcons.slides,
  },
  results: {
    id: 'results',
    contentState: MenuState.HIDDEN,
    labelState: MenuState.VISIBLE,
    selected: false,
    alignment: 'end',
    order: 2,
    icon: MenuIcons.results,
  }
};
