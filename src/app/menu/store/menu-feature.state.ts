import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromMenu from '@menu/store/menu/menu.reducer';

export const MENU_MODULE_FEATURE_KEY = 'menuModuleFeature';

export const selectMenuFeatureState = createFeatureSelector<State>(
  MENU_MODULE_FEATURE_KEY
);

export interface State {
  [fromMenu.MENU_FEATURE_KEY]: fromMenu.State,
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromMenu.MENU_FEATURE_KEY]: fromMenu.reducer,
  })(state, action);
}
