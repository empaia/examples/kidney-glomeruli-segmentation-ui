import { createReducer } from '@ngrx/store';
import { immerOn } from 'ngrx-immer/store';
import { MENU, MenuEntity, MenuState, MenuType } from '@menu/models/menu.models';
import * as MenuActions from './menu.actions';


export const MENU_FEATURE_KEY = 'menu';

export interface State {
  menu: MenuEntity[];
  showAll: boolean;
}

export const initialState: State = {
  menu: Object.values(MENU),
  showAll: true,
};


export const reducer = createReducer(
  initialState,
  immerOn(MenuActions.openMenu, (state, { id }) => {
    const index = getMenuItemIndex(id, state.menu);
    selectAndOpenItem(index, MenuState.VISIBLE, state.menu);
    deselectAndMinimizeOthers(id, state.menu);
  }),
  immerOn(MenuActions.showLabel, (state, { id }) => {
    const index = getMenuItemIndex(id, state.menu);
    state.menu[index].labelState = MenuState.VISIBLE;
  }),
  immerOn(MenuActions.showAllMenusToggle, (state) => {
    state.showAll = !state.showAll;
  }),
);

function getMenuItemIndex(id: MenuType, menu: MenuEntity[]): number {
  return menu.findIndex(i => i.id === id);
}

function getMenuItemsAbove(id: MenuType, menu: MenuEntity[]): MenuEntity[] {
  const entity = getMenuItem(id, menu);
  const onSameSide = menu.filter(i => i.alignment === entity.alignment);
  return onSameSide.filter(i => i.order < entity.order);
}

function getMenuItem(id: MenuType, menu: MenuEntity[]): MenuEntity {
  const index = menu.findIndex(i => i.id === id);
  return menu[index];
}

function deselectAndMinimizeOthers(id: MenuType, menu: MenuEntity[]): void {
  deselectOtherMenus(id, menu);
  minimizeLabelsAbove(id, menu);
}

// hide all other menus in same column / on same side
function minimizeLabelsAbove(id: MenuType, menu: MenuEntity[]): void {
  const above = getMenuItemsAbove(id, menu);
  for (const item of above) {
    item.labelState = MenuState.VISIBLE;
    item.contentState = MenuState.HIDDEN;
  }
}

// deselect all other menus if menu (id) is selected
function deselectOtherMenus(id: MenuType, menu: MenuEntity[]): void {
  const clickedItem = getMenuItem(id, menu);
  const others = menu.filter(
    i => i.id !== id
      && i.alignment === clickedItem.alignment
  );
  for (const item of others) {
    item.selected = false;
    item.contentState = MenuState.HIDDEN;
  }
}

function selectAndOpenItem(index: number, state: MenuState, menu: MenuEntity[]): void {
  menu[index].selected = true;
  menu[index].labelState = state;
  menu[index].contentState = state;
}

