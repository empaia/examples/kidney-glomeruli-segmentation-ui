import { Observable } from 'rxjs';

import { ClassesEffects } from '@classes/store';
import { createServiceFactory, mockProvider, SpectatorService } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { DataPersistence, NxModule } from '@nrwl/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DataPanelService } from '@api/wbs-api/services/data-panel.service';
import { ClassNameConversionService } from '@classes/services/class-name-conversion.service';

describe('ClassesEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<ClassesEffects>;
  const createService = createServiceFactory({
    service: ClassesEffects,
    imports: [
      NxModule.forRoot(),
      HttpClientTestingModule,
    ],
    providers: [
      ClassesEffects,
      DataPersistence,
      provideMockActions(() => actions$),
      provideMockStore(),
      mockProvider(DataPanelService),
      mockProvider(ClassNameConversionService),
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
