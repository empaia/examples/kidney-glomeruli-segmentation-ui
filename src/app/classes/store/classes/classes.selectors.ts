import {  createSelector } from '@ngrx/store';
import {
  selectClassesFeatureState,
  State as ModuleState,
} from '../classes-feature.state';
import { classesAdapter } from '@classes/store/classes/classes.reducer';

const {
  selectAll
} = classesAdapter.getSelectors();

export const selectClassesState = createSelector(
  selectClassesFeatureState,
  (state: ModuleState) => state.classes
);

export const selectAllClasses = createSelector(
  selectClassesState,
  selectAll,
);
