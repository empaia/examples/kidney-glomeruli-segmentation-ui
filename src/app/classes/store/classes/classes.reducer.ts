import { Action, createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { ClassEntity } from '@classes/store/classes/classes.models';
import * as ClassesActions from './classes.actions';


export const CLASSES_FEATURE_KEY = 'classes';

export type State = EntityState<ClassEntity>;

export const classesAdapter = createEntityAdapter<ClassEntity>();

export const initialState: State = classesAdapter.getInitialState();

const classesReducer = createReducer(
  initialState,
  on(ClassesActions.loadClassNamespacesSuccess, (state, { classes }): State =>
    classesAdapter.setAll(classes, {
      ...state,
    })
  ),
  on(ClassesActions.clearClassesEAD, (state): State =>
    classesAdapter.removeAll({
      ...state,
    })
  ),
);

export function reducer(state: State | undefined, action: Action) {
  return classesReducer(state, action);
}
