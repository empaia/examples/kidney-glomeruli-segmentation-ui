import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import * as ClassesActions from './classes.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { map, catchError, concatMap } from 'rxjs/operators';
import { DataPanelService } from '@api/wbs-api/services/data-panel.service';
import { ClassNameConversionService } from '@classes/services/class-name-conversion.service';
import { Store } from '@ngrx/store';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { of } from 'rxjs';


@Injectable()
export class ClassesEffects {

  prepareLoadClassNamespaces$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeSuccess),
      concatLatestFrom(() => this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())),
      map(([, scopeId]) => scopeId),
      map(scopeId => ClassesActions.loadClassNamespaces({ scopeId }))
    );
  });

  loadClassNamespaces$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClassesActions.loadClassNamespaces),
      concatMap(action =>
        this.dataPanelService.scopeIdClassNamespacesGet({
          scope_id: action.scopeId
        }).pipe(
          map(response => this.classNameConverter.getClasses(response)),
          map(classes => ClassesActions.loadClassNamespacesSuccess({ classes })),
          catchError(error => of(ClassesActions.loadClassNamespacesFailure({ error })))
        )
      )
    )
  });

  clearClassesEad$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScope),
      map(() => ClassesActions.clearClassesEAD())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly dataPanelService: DataPanelService,
    private readonly classNameConverter: ClassNameConversionService,
    private readonly store: Store,
  ) {}

}
