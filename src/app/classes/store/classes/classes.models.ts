import { ClassesDict } from '@api/wbs-api/models/classes-dict';

export interface ClassEntity {
  id: string;
}

export interface EmpaiaAppDescription {
  name: string;
  name_short: string;
  namespace: string;
  description: string;
  inputs: object;
  outputs: object;
  classes?: object;
}

export interface ClassDictionaries {
  [p: string]: ClassesDict;
}
