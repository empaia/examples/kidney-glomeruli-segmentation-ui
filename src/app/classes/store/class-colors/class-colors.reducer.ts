import { Action, createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { ClassColorEntity } from '@classes/store/class-colors/class-colors.models';
import * as ClassColorsActions from './class-colors.actions';


export const CLASS_COLORS_FEATURE_KEY = 'classColors';

export type State = EntityState<ClassColorEntity>;

export const classColorsAdapter = createEntityAdapter<ClassColorEntity>();

export const initialState: State = classColorsAdapter.getInitialState();

const classColorReducer = createReducer(
  initialState,
  on(ClassColorsActions.storeClassColorMapping, (state, { colorMapping }): State =>
    classColorsAdapter.setAll(Object.entries(colorMapping).map(entry => ({id: entry[0], color: entry[1] })), {
      ...state
    })
  ),
);

export function reducer(state: State | undefined, action: Action) {
  return classColorReducer(state, action);
}
