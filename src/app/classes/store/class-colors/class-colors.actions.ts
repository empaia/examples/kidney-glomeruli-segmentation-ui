import { createAction, props } from '@ngrx/store';
import { AnnotationClassColor } from '@classes/store';

export const storeClassColorMapping = createAction(
  '[Class-Colors] Store Class Color Mapping',
  props<{ colorMapping: AnnotationClassColor }>()
);
