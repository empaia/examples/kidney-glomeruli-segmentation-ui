import { createSelector } from '@ngrx/store';
import {
  selectClassesFeatureState,
  State as ModuleState
} from '../classes-feature.state';
import { classColorsAdapter } from './class-colors.reducer';

const {
  selectEntities
} = classColorsAdapter.getSelectors();

export const selectClassColorState = createSelector(
  selectClassesFeatureState,
  (state: ModuleState) => state.classColors
);

export const selectAllClassColors = createSelector(
  selectClassColorState,
  (state) => selectEntities(state)
);
