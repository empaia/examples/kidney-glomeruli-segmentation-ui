import * as ClassesActions from './classes/classes.actions';
import * as ClassesFeature from './classes/classes.reducer';
import * as ClassesSelectors from './classes/classes.selectors';
export * from './classes/classes.effects';
export * from './classes/classes.models';

export { ClassesActions, ClassesFeature, ClassesSelectors };

import * as ClassColorsActions from './class-colors/class-colors.actions';
import * as ClassColorsFeature from './class-colors/class-colors.reducer';
import * as ClassColorsSelectors from './class-colors/class-colors.selectors';
export * from './class-colors/class-colors.models';

export { ClassColorsActions, ClassColorsFeature, ClassColorsSelectors };
