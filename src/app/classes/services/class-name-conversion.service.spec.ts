import { ClassNameConversionService } from './class-name-conversion.service';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';

describe('ClassNameConversionService', () => {
  let spectator: SpectatorService<ClassNameConversionService>;
  const createService = createServiceFactory({
    service: ClassNameConversionService,
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });
});
