/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';
import { ClassesDict } from '@api/wbs-api/models/classes-dict';
import { ClassDictionaries, ClassEntity } from '@classes/store';

@Injectable({
  providedIn: 'root'
})
export class ClassNameConversionService {

  public getClassEntries(obj: ClassDictionaries | ClassesDict): string[] {
    return Object.entries(obj).map(([k ,v]) => {
      return v instanceof Object ? v['name'] ? k: k + '.' + this.getClassEntries(v): k;
    });
  }

  public getNamespaces(classDictionaries: ClassDictionaries): string[] {
    return this.getClassEntries(classDictionaries).map(e => e.slice(0, e.lastIndexOf('.')));
  }

  public getClassNames(classes: ClassDictionaries | ClassesDict): any[] {
    return Object.entries(classes).map(([k, v]) => v instanceof Object ? v['name'] ? k : this.getClassNames(v) : k);
  }

  private flatClass(r: any[], a: any[]): any[] {
    if (Array.isArray(a[0])) {
      return a.reduce(this.flatClass.bind(this), r);
    }
    r.push(a);
    return r;
  }

  public flatClassNames(classNames: any[]): any[] {
    return classNames.reduce(this.flatClass.bind(this), []);
  }

  public getClasses(classDictionaries: ClassDictionaries): ClassEntity[] {
    const namespaces = this.getNamespaces(classDictionaries);
    const classNames = this.flatClassNames(this.getClassNames(classDictionaries));
    const classes: ClassEntity[] = [];

    namespaces.forEach((namespace, i) => {
      classNames[i].forEach((c: string) => {
        classes.push({ id: namespace +'.' + c });
      });
    });

    return classes;
  }
}
