import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NxModule } from '@nrwl/angular';

import { ApiModule as WbsApiModule } from '@api/wbs-api/api.module';

import { AppComponent } from '@core/containers/app/app.component';
import { AppRouterModule } from '@router/app-router.module';
import { AnnotationsModule } from '@annotations/annotations.module';
import { CoreModule } from '@core/core.module';
import { JobsModule } from '@jobs/jobs.module';
import { MaterialModule } from '@material/material.module';
import { MenuModule } from '@menu/menu.module';
import { SharedModule } from '@shared/shared.module';
import { MetaReducer, StoreModule } from '@ngrx/store';
import { environment } from '../environments/environment';
import { storeFreeze } from 'ngrx-store-freeze';
import { EffectsModule } from '@ngrx/effects';
import { AuthenticationInterceptor } from '@core/services/interceptors/authentication.interceptor';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ScopeModule } from '@scope/scope.module';
import { TokenModule } from '@token/token.module';
import { WbsUrlModule } from '@wbsUrl/wbs-url.module';
import { ClassesModule } from '@classes/classes.module';

export const metaReducers: MetaReducer<object>[] = !environment.production
  ? [storeFreeze]
  : [];

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NxModule.forRoot(),
    StoreModule.forRoot({},
      {
        metaReducers,
        runtimeChecks: {
          strictActionImmutability: true,
          strictStateImmutability: true,
          strictActionWithinNgZone: true
        }
      }
    ),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument({
      name: 'Sample-App Store'
    }) : [],
    WbsApiModule,
    AnnotationsModule,
    AppRouterModule,
    ClassesModule,
    CoreModule,
    JobsModule,
    MaterialModule,
    MenuModule,
    SharedModule,
    ScopeModule,
    TokenModule,
    WbsUrlModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
