import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { reducers, SLIDES_MODULE_FEATURE_KEY } from '@slides/store/slides-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { SlidesEffects, SlidesImagesEffects } from '@slides/store';
import { SlidesContainerComponent } from './containers/slides-container/slides-container.component';
import { SlidesLabelComponent } from './components/slides-label/slides-label.component';
import { SharedModule } from '@shared/shared.module';
import { MaterialModule } from '@material/material.module';
import { SlidesComponent } from './components/slides/slides.component';
import { SlideImageItemComponent } from './components/slide-image-item/slide-image-item.component';
import { SlideItemComponent } from './components/slide-item/slide-item.component';
import { ReactiveComponentModule } from '@ngrx/component';



@NgModule({
  declarations: [
    SlidesContainerComponent,
    SlidesLabelComponent,
    SlidesComponent,
    SlideImageItemComponent,
    SlideItemComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveComponentModule,
    SharedModule,
    StoreModule.forFeature(
      SLIDES_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      SlidesEffects,
      SlidesImagesEffects,
    ]),
  ],
  exports: [
    SlidesContainerComponent
  ]
})
export class SlidesModule { }
