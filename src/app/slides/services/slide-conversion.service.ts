import { Injectable } from '@angular/core';
import { ImageInfo, Slide } from '@empaia/slide-viewer';
import { SlideLevel } from '@api/wbs-api/models/slide-level';
import { SlideInfo } from '@api/wbs-api/models/slide-info';
import { WbsServerTileLoader } from '@slides/models/wbs-server-tile-loader';
import { AccessTokenService } from '@token/services/access-token.service';
import { WbsUrlService } from '@wbsUrl/services/wbs-url.service';
import { ScopeService } from '@scope/services/scope.service';

@Injectable({
  providedIn: 'root'
})
export class SlideConversionService {

  constructor(
    private accessTokenService: AccessTokenService,
    private scopeService: ScopeService,
    private wbsUrlService: WbsUrlService,
  ) { }

  public fromWbsApiType(wbsSlide: SlideInfo): Slide {
    const { resolutions, hasArtificialLevel } = this.calcResolutions(wbsSlide.levels);
    const imageInfo = this.createImageInfo(wbsSlide, resolutions);

    return {
      slideId: wbsSlide.id,
      resolver: this.createLoader(
        wbsSlide.id,
        hasArtificialLevel,
        imageInfo.numberOfLevels
      ),
      imageInfo,
    };
  }

  private createLoader(
    id: string,
    hasArtificialLevel: boolean,
    numberOfLevels: number
  ): WbsServerTileLoader {
    return new WbsServerTileLoader(
      id,
      this.wbsUrlService.wbsUrlComplete + `/${this.scopeService.scopeId}/slides/`,
      hasArtificialLevel,
      numberOfLevels,
      [{
        key: this.accessTokenService.headerKey,
        value: this.accessTokenService.getAccessToken.bind(this.accessTokenService),
      }],
    );
  }

  private createImageInfo(wbsSlide: SlideInfo, resolutions: number[]): ImageInfo {
    const npp = this.calcNpp(wbsSlide);
    const tileSize = this.calcTileSize(wbsSlide);

    return {
      width: wbsSlide.extent.x,
      height: wbsSlide.extent.y,
      numberOfLevels: wbsSlide.num_levels,
      tileSize,
      npp,
      resolutions,
    };
  }

  private calcResolutions(wbsLevels: Array<SlideLevel>): { resolutions: Array<number>, hasArtificialLevel: boolean } {
    // add an artifical level here if our wsi has only levels with a downsample factor below 128
    let hasArtificialLevel = false;
    const resolutions = [];
    const maximum = Math.max(...wbsLevels.map(it => it.downsample_factor));
    if (maximum < 128) {
      resolutions.push(128);
      hasArtificialLevel = true;
    }
    wbsLevels.reverse().forEach(it => resolutions.push(it.downsample_factor));
    return { resolutions, hasArtificialLevel };
  }

  private calcTileSize(wbsSlide: SlideInfo): number {
    return (wbsSlide.tile_extent.x + wbsSlide.tile_extent.y) / 2;
  }

  private calcNpp(wbsSlide: SlideInfo): number {
    return wbsSlide.pixel_size_nm.x ? wbsSlide.pixel_size_nm.x : 0;
  }
}
