import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { MenuEntity, MenuState } from '@menu/models/menu.models';
import { Store } from '@ngrx/store';
import { MenuSelectors } from '@menu/store';
import { SlideEntity } from '@slides/store/slides/slides.models';
import { SlideImage } from '@slides/store/slides-images/slides-images.model';
import { SelectionError } from '@menu/models/ui.model';
import { SlidesActions, SlidesImagesSelectors, SlidesSelectors } from '@slides/store';

@Component({
  selector: 'app-slides-container',
  templateUrl: './slides-container.component.html',
  styleUrls: ['./slides-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlidesContainerComponent {
  public menuState = MenuState;
  public slidesMenu$: Observable<MenuEntity>;
  public slide$: Observable<SlideEntity | undefined>;
  public slides$: Observable<SlideEntity[]>;
  public slideImages$: Observable<SlideImage[]>;
  public loaded$: Observable<boolean>;
  public selectionError$: Observable<SelectionError | undefined>;

  constructor(private store: Store) {
    this.slidesMenu$ = this.store.select(MenuSelectors.selectSlidesMenu);
    this.slide$ = this.store.select(SlidesSelectors.selectSelectedSlide);
    this.slides$ = this.store.select(SlidesSelectors.selectAllSlides);
    this.slideImages$ = this.store.select(SlidesImagesSelectors.selectAllSlidesImages);
    this.loaded$ = this.store.select(SlidesSelectors.selectSlidesLoaded);
    this.selectionError$ = this.store.select(SlidesSelectors.selectSelectionError);
  }

  public selectSlide(id: string): void {
    this.store.dispatch(SlidesActions.selectSlide({ id }));
  }
}
