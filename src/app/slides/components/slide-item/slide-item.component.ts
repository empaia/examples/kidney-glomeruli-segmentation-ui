import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { SlideEntity } from '@slides/store/slides/slides.models';
import { SlideImage } from '@slides/store/slides-images/slides-images.model';

@Component({
  selector: 'app-slide-item',
  templateUrl: './slide-item.component.html',
  styleUrls: ['./slide-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlideItemComponent {
  @Input() slide!: SlideEntity;
  @Input() slideImage!: SlideImage;
  @Input() selectedSlide!: string;

  @Output() selectSlide = new EventEmitter<string>();

  public onSlideSelect(event: MouseEvent, slideId: string): void {
    this.selectSlide.emit(slideId);
    event.stopImmediatePropagation();
  }
}
