import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { SlideImage } from '@slides/store/slides-images/slides-images.model';

enum ImageType {
  LABEL = 'label',
  THUMBNAIL = 'thumbnail'
}

@Component({
  selector: 'app-slide-image-item',
  templateUrl: './slide-image-item.component.html',
  styleUrls: ['./slide-image-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlideImageItemComponent {
  @Input() imageType!: ImageType;
  @Input() slideImage!: SlideImage;
  @Input() fallbackImage!: string;
  @Input() isDeleted!: boolean;
}
