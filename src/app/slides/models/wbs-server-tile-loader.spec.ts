import { WbsServerTileLoader } from './wbs-server-tile-loader';

describe('WbsServerTileLoader', () => {
  it('should create an instance', () => {
    expect(new WbsServerTileLoader(
      'IMAGE-ID',
      'IMAGE-SOURCE-URL',
      false,
      1,
      []
    )).toBeTruthy();
  });
});
