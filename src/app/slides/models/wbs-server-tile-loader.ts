import { TileResolver } from '@empaia/slide-viewer';
import { ImageTile, Tile } from 'ol';
import TileState from 'ol/TileState';
import { addTokenListener, removeTokenListener, requestNewToken } from '@empaia/vendor-app-communication-interface';

type Headers = Array<{ key: string, value: () => string }>;
export class WbsServerTileLoader implements TileResolver {
  private static expiredTokens = new Set<string>();
  public startZoom!: number;

  constructor(
    public imageId: string,
    public imageSourceUrl: string,
    public hasArtificialLevel = false,
    public numberOfLevels: number,
    public headers: Headers,
  ) { }

  private static sendTokenRequest(expiredToken: string | undefined): void {
    if (expiredToken && !WbsServerTileLoader.expiredTokens.has(expiredToken)) {
      WbsServerTileLoader.expiredTokens.add(expiredToken);
      requestNewToken();
    }
  }

  private static clearExpiredTokens(): void {
    WbsServerTileLoader.expiredTokens.clear();
  }

  // eslint-disable-next-line
  tileResolverFn: any = null;

  tileLoaderFn = (tile: Tile): void => {
    const tileCoords = tile.getTileCoord();
    const level = this.numberOfLevels - tileCoords[0] - (this.hasArtificialLevel ? 0 : 1);

    if (isNaN(level)) {
      throw new Error('Error in Tile Resolver - Tile level invalid!');
    }

    if (level < 0) { return; }
    const x = tileCoords[1];
    const y = tileCoords[2];

    const srcUrl = `${this.imageSourceUrl}${this.imageId}/tile/level/${level}/tile/${x}/${y}`;

    const client = new XMLHttpRequest();
    client.open('GET', srcUrl);

    const headers = this.headers;
    for (const header of this.headers) {
      client.setRequestHeader(header.key, header.value());
    }

    let listenerIndex: number;
    const lastToken = this.headers.find(h => h.key === 'Authorization')?.value();

    client.responseType = 'blob';

    client.addEventListener('loadend', function (_event: ProgressEvent<XMLHttpRequestEventTarget>) {
      if (client.status === 200) {
        const data = this.response;
        if (data !== undefined) {
          ((tile as ImageTile).getImage() as HTMLImageElement).src = URL.createObjectURL(data);
          WbsServerTileLoader.clearExpiredTokens();
          if (listenerIndex !== undefined) { removeTokenListener(listenerIndex); }
        } else {
          tile.setState(TileState.ERROR);
        }
      } else if (client.status === 401 || client.status === 403) {
        listenerIndex = addTokenListener(token => {
          const accessToken = 'Bearer ' + token.value;
          if (lastToken !== accessToken) {
            client.open('GET', srcUrl);
            for (const header of headers) {
              client.setRequestHeader(header.key, header.key === 'Authorization' ? accessToken : header.value());
            }
            client.responseType = 'blob';
            client.send();
          }
        });
        WbsServerTileLoader.sendTokenRequest(lastToken);
        tile.setState(TileState.ERROR);
      } else {
        tile.setState(TileState.ERROR);
      }
    }, { passive: false });
    client.addEventListener('error', function () {
      tile.setState(TileState.ERROR);
    }, { passive: false });

    client.send();
  };
}
