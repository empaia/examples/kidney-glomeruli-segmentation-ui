import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromSlides from '@slides/store/slides/slides.reducer';
import * as fromSlidesImages from '@slides/store/slides-images/slides-images.reducer';

export const SLIDES_MODULE_FEATURE_KEY = 'slidesModuleFeature';

export const selectSlideFeatureState = createFeatureSelector<State>(
  SLIDES_MODULE_FEATURE_KEY
);

export interface State {
  [fromSlides.SLIDES_FEATURE_KEY]: fromSlides.State,
  [fromSlidesImages.SLIDES_IMAGES_FEATURE_KEY]: fromSlidesImages.State,
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromSlides.SLIDES_FEATURE_KEY]: fromSlides.reducer,
    [fromSlidesImages.SLIDES_IMAGES_FEATURE_KEY]: fromSlidesImages.reducer,
  })(state, action);
}
