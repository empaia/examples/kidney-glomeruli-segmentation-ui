import * as fromSlidesImages from './slides-images.actions';

describe('loadSlidesImages', () => {
  it('should return an action', () => {
    expect(fromSlidesImages.loadSlideLabel({
      scopeId: 'SCOPE-ID',
      slideId: 'SLIDE-ID',
      maxWidth: -1,
      maxHeight: -1,
    }).type).toBe('[Slides-Images] Load Slide Label');
  });
});
