import { createAction, props } from '@ngrx/store';
import { SlideImage, SlideImageFormat } from '@slides/store/slides-images/slides-images.model';
import { HttpError } from '@menu/models/ui.model';


export const loadSlideLabel = createAction(
  '[Slides-Images] Load Slide Label',
  props<{
    scopeId: string,
    slideId: string,
    maxWidth: number,
    maxHeight: number,
    imageFormat?: SlideImageFormat,
    imageQuality?: number,
  }>()
);

export const loadSlideLabelSuccess = createAction(
  '[Slides-Images] Load Slide Label Success',
  props<{ slideImage: SlideImage }>()
);

export const loadSlideLabelFailure = createAction(
  '[Slides-Images] Load Slide Label Failure',
  props<{ slideId: string, error: HttpError }>()
);

export const loadManySlidesLabels = createAction(
  '[Slides-Images] Load Many Slides Images',
  props<{
    scopeId: string,
    slideIds: string[],
    maxWidth: number,
    maxHeight: number,
    imageFormat?: SlideImageFormat,
    imageQuality?: number,
  }>()
);

export const loadSlideThumbnail = createAction(
  '[Slides-Images] Load Slide Thumbnail',
  props<{
    scopeId: string,
    slideId: string,
    maxWidth: number,
    maxHeight: number,
    imageFormat?: SlideImageFormat,
    imageQuality?: number,
  }>()
);

export const loadSlideThumbnailSuccess = createAction(
  '[Slides-Images] Load Slide Thumbnail Success',
  props<{ slideImage: SlideImage }>()
);

export const loadSlideThumbnailFailure = createAction(
  '[Slides-Images] Load Slide Thumbnail Failure',
  props<{ slideId: string, error: HttpError }>()
);

export const loadManySlidesThumbnails = createAction(
  '[Slides-Images] Load Many Slides Images',
  props<{
    scopeId: string,
    slideIds: string[],
    maxWidth: number,
    maxHeight: number,
    imageFormat?: SlideImageFormat,
    imageQuality?: number,
  }>()
);

export const loadManySlidesLabelsAndThumbnails = createAction(
  '[Slides-Images] Load Many Slides Labels And Thumbnails',
  props<{
    slideIds: string[],
    maxWidth: number,
    maxHeight: number,
    imageFormat?: SlideImageFormat,
    imageQuality?: number,
  }>()
);

export const prepareClearImages = createAction(
  '[Slides-Images] Prepare Clear Images'
);

export const clearAllImages = createAction(
  '[Slides-Images] Clear All Images'
);
