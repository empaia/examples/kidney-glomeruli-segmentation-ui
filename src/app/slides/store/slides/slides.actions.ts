import { createAction, props } from '@ngrx/store';
import { HttpError } from '@menu/models/ui.model';
import { SlideEntity } from '@slides/store/slides/slides.models';
import { Slide } from '@empaia/slide-viewer';

export const loadSlides = createAction(
  '[APP/Slides] Load Slides',
  props<{ scopeId: string }>()
);

export const loadSlidesSuccess = createAction(
  '[APP/Slides] Load Slides Success',
  props<{ slides: SlideEntity[] }>()
);

export const loadSlidesFailure = createAction(
  '[App/Slides] Load Slides Failure',
  props<{ error: HttpError }>()
);

export const loadSlideImageInfoSuccess = createAction(
  '[APP/Slide] Load Slide Image Info Success',
  props<{ slideImage: Slide }>()
);

export const selectSlide = createAction(
  '[APP/Slides] Select Slide',
  props<{ id: string | undefined }>()
);

export const clearSlides = createAction(
  '[APP/Slides] Clear Slides'
);
