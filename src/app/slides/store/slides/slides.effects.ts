import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import * as SlidesActions from './slides.actions';
import * as SlidesFeature from './slides.reducer';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as TokenActions from '@token/store/token/token.actions';
import * as TokenSelectors from '@token/store/token/token.selectors';
import { SlidesPanelService } from '@api/wbs-api/services/slides-panel.service';
import { catchError, distinctUntilChanged, filter, map, mergeMap, retryWhen, concatMap } from 'rxjs/operators';
import { SlideEntity } from '@slides/store/slides/slides.models';
import { SlideConversionService } from '@slides/services/slide-conversion.service';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { requestNewToken } from '@empaia/vendor-app-communication-interface';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';



@Injectable()
export class SlidesEffects {

  // start loading slides after the scope id was set
  startLoadingSlides$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        ScopeActions.setScope,
        TokenActions.setAccessToken
      ),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(TokenSelectors.selectAccessToken)
      ]),
      filter(([, _scopeId, token]) => !!token),
      distinctUntilChanged((prev, curr) => prev[1] === curr[1]),
      map(([, scopeId]) => scopeId),
      map(scopeId => SlidesActions.loadSlides({ scopeId }))
    );
  });

  loadSlides$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.loadSlides),
      concatMap(action => this.slidePanelService.scopeIdSlidesGet({
          scope_id: action.scopeId
        }).pipe(
          // retry to load the selected slide after the token is expired
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          map(slides => slides.items),
          map(slideViews => slideViews.map(v => ({
            id: v.id,
            dataView: v,
            disabled: false
          } as SlideEntity)
          )),
          map(slides => SlidesActions.loadSlidesSuccess({ slides })),
          catchError(error => of(SlidesActions.loadSlidesFailure({ error })))
        )
      )
    )
  });

  selectSlide$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide),
      // map(action => action.id),
      filter(action => !!action.id),
      map(action => action.id),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectScopeId)
      ),
      mergeMap(([id, scopeId]) =>
        this.slidePanelService.scopeIdSlidesSlideIdInfoGet({
          slide_id: id as string,
          scope_id: scopeId as string,
        }).pipe(
          map(slideInfo => this.slideConverter.fromWbsApiType(slideInfo)),
          map(slideImage => SlidesActions.loadSlideImageInfoSuccess({ slideImage })),
          // retry to load the selected slide after the token is expired
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          catchError(error => of(SlidesActions.loadSlidesFailure({ error })))
        )
      ),
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly slidePanelService: SlidesPanelService,
    private readonly slideConverter: SlideConversionService,
    private readonly store: Store,
  ) {}

}
