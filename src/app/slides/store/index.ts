import * as SlidesActions from './slides/slides.actions';
import * as SlidesSelectors from './slides/slides.selectors';
import * as SlidesFeature from './slides/slides.reducer';

export * from './slides/slides.effects';

export {
  SlidesActions,
  SlidesSelectors,
  SlidesFeature
};

import * as SlidesImagesActions from './slides-images/slides-images.actions';
import * as SlidesImagesFeature from './slides-images/slides-images.reducer';
import * as SlidesImagesSelectors from './slides-images/slides-images.selectors';
export * from './slides-images/slides-images.effects';

export {
  SlidesImagesActions,
  SlidesImagesFeature,
  SlidesImagesSelectors,
};
