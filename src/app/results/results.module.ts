import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { reducers, RESULTS_MODULE_FEATURE_KEY } from '@results/store/resutls-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { ResultsComponent } from './components/results/results.component';
import { ResultsLabelComponent } from './components/results-label/results-label.component';
import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';
import { ResultsContainerComponent } from './containers/results-container/results-container.component';
import { ResultsEffects } from '@results/store';
import { ResultItemComponent } from './components/result-item/result-item.component';



@NgModule({
  declarations: [
    ResultsComponent,
    ResultsLabelComponent,
    ResultsContainerComponent,
    ResultItemComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    StoreModule.forFeature(
      RESULTS_MODULE_FEATURE_KEY,
      reducers,
    ),
    EffectsModule.forFeature([
      ResultsEffects,
    ])
  ],
  exports: [
    ResultsContainerComponent
  ]
})
export class ResultsModule { }
