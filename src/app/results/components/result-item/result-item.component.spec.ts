import { ResultItemComponent } from './result-item.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MaterialModule } from '@material/material.module';
import { MockComponents } from 'ng-mocks';
import { JobStatusComponent } from '@shared/components/job-status/job-status.component';

describe('ResultItemComponent', () => {
  let spectator: Spectator<ResultItemComponent>;
  const createComponent = createComponentFactory({
    component: ResultItemComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        JobStatusComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
