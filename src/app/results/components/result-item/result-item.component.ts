import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges
} from '@angular/core';
import { FINISHED_JOB_STATES, ResultEntity } from '@results/store/results/results.models';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { JobSelector } from '@jobs/store/jobs/jobs.models';
import { AnnotationEntity } from '@empaia/slide-viewer';
import { ClassColorEntity } from '@classes/store';
import { Dictionary } from '@ngrx/entity';


@Component({
  selector: 'app-result-item',
  templateUrl: './result-item.component.html',
  styleUrls: ['./result-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultItemComponent implements OnChanges {
  @Input() result!: ResultEntity;
  @Input() selected!: boolean;
  @Input() classColorEntities!: Dictionary<ClassColorEntity>;

  @Output() changeSelection = new EventEmitter<JobSelector>();
  @Output() zoomToAnnotation = new EventEmitter<AnnotationEntity>();
  @Output() interruptJobExecution = new EventEmitter<string>();

  jobInfo = '';
  readonly FINISHED_JOB_STATES = FINISHED_JOB_STATES;

  ngOnChanges(changes: SimpleChanges) {
    if (changes['result']) {
      this.jobInfo = `Job-ID: ${this.result.job.id}`;
      this.jobInfo += this.result.job.error_message ? `\nError: ${this.result.job.error_message}` : '';
    }
  }

  onCheckboxChange(change: MatCheckboxChange): void {
    const jobSelector: JobSelector = {
      id: this.result.job.id,
      checked: change.checked,
    };
    this.changeSelection.emit(jobSelector);
  }

  onJobStop(id: string): void {
    this.interruptJobExecution.emit(id);
  }
}
