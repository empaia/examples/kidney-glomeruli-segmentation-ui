import { ResultsLabelComponent } from './results-label.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MenuLabelComponent } from '@shared/components/menu-label/menu-label.component';
import { IconCheckboxComponent } from '@shared/components/icon-checkbox/icon-checkbox.component';
import { MockComponents } from 'ng-mocks';

describe('ResultsLabelComponent', () => {
  let spectator: Spectator<ResultsLabelComponent>;
  const createComponent = createComponentFactory({
    component: ResultsLabelComponent,
    declarations: [
      MockComponents(
        MenuLabelComponent,
        IconCheckboxComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
