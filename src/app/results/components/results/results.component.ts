import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { SelectionError } from '@menu/models/ui.model';
import { MenuType } from '@menu/models/menu.models';
import { ResultEntity } from '@results/store/results/results.models';
import { Dictionary } from '@ngrx/entity';
import { JobSelector } from '@jobs/store/jobs/jobs.models';
import { AnnotationEntity } from '@empaia/slide-viewer';
import { ClassColorEntity } from '@classes/store';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsComponent {
  @Input() results!: ResultEntity[];
  @Input() jobsSelections!: Dictionary<JobSelector>;
  @Input() classColorEntities!: Dictionary<ClassColorEntity>;
  @Input() selectionMissing!: SelectionError;

  @Output() zoomToAnnotation = new EventEmitter<AnnotationEntity>();
  @Output() changeJobSelection = new EventEmitter<JobSelector>();
  @Output() errorClicked = new EventEmitter<MenuType>();
  @Output() interruptJobExecution = new EventEmitter<string>();
}
