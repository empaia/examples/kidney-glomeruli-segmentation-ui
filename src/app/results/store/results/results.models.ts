import { IntegerPrimitive } from '@api/wbs-api/models/integer-primitive';
import { FloatPrimitive } from '@api/wbs-api/models/float-primitive';
import { BoolPrimitive } from '@api/wbs-api/models/bool-primitive';
import { StringPrimitive } from '@api/wbs-api/models/string-primitive';
import { Job } from '@api/wbs-api/models/job';
import { AnnotationEntity } from '@empaia/slide-viewer';
import { JobStatus } from '@api/wbs-api/models/job-status';

export type PrimitiveEntity = IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive;

export interface ResultEntity {
  job: Job;
  annotation: AnnotationEntity;
  primitives: PrimitiveEntity[];
}

export const FINISHED_JOB_STATES = [JobStatus.Completed, JobStatus.Error, JobStatus.Failed, JobStatus.Incomplete, JobStatus.Timeout];
