import { Observable } from 'rxjs';
import { ResultsEffects } from './results.effects';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { DataPersistence, NxModule } from '@nrwl/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';

describe('ResultsEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<ResultsEffects>;
  const createService = createServiceFactory({
    service: ResultsEffects,
    imports: [
      NxModule.forRoot(),
      HttpClientTestingModule,
    ],
    providers: [
      ResultsEffects,
      DataPersistence,
      provideMockActions(() => actions$),
      provideMockStore()
    ]
  });

  beforeEach(() => spectator = createService());


  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
