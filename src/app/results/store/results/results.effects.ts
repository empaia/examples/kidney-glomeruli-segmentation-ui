import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { EadService } from '@core/services/ead.service';
import { DataPanelService } from '@api/wbs-api/services/data-panel.service';
import { ResultsFeature } from '..';
import * as ResultsActions from './results.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as AnnotationsActions from '@annotations/store/annotations/annotations.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as AnnotationsSelectors from '@annotations/store/annotations/annotations.selectors';
import * as JobsSelectors from '@jobs/store/jobs/jobs.selectors';
import { filter, map, catchError, concatMap } from 'rxjs/operators';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { of } from 'rxjs';



@Injectable()
export class ResultsEffects {

  // clear results on slide selection
  clearResultsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        SlidesActions.selectSlide,
        SlidesActions.clearSlides,
      ),
      map(() => ResultsActions.clearResults())
    );
  });

  // load primitive results after input annotations were fetched
  loadPrimitives$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        AnnotationsActions.loadInputAnnotationsSuccess
      ),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(AnnotationsSelectors.selectAllAnnotationIds).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllJobIds),
      ]),
      filter(([, _scopeId, annotationIds, jobIds]) => !!jobIds.length && !!annotationIds.length),
      map(([, scopeId, annotationIds, jobIds]) =>
        ResultsActions.loadResults({
          scopeId,
          annotationIds,
          jobIds,
        })
      )
    );
  });

  loadResults$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ResultsActions.loadResults),
        concatMap(action => this.dataPanel.scopeIdPrimitivesQueryPut({
          scope_id: action.scopeId,
          body: {
            references: action.annotationIds,
            jobs: action.jobIds,
          }
        }).pipe(
          map(results => results.items),
          map(results => ResultsActions.loadResultsSuccess({ results })),
          catchError(error => of(ResultsActions.loadResultsFailure({ error })))
        )
      )
    )
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataPanel: DataPanelService,
    private readonly eadService: EadService,
  ) {}

}
