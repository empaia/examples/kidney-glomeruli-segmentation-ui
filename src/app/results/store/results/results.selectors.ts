import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectResultsFeatureState,
} from '../resutls-feature.state';
import { resultsAdapter } from '@results/store/results/results.reducer';
import * as JobsSelectors from '@jobs/store/jobs/jobs.selectors';
import * as AnnotationsSelectors from '@annotations/store/annotations/annotations.selectors';
import { ResultEntity } from '@results/store/results/results.models';
import { AnnotationEntity } from '@empaia/slide-viewer';

const {
  selectAll,
  selectEntities
} = resultsAdapter.getSelectors();

export const selectResultsState = createSelector(
  selectResultsFeatureState,
  (state: ModuleState) => state.results
);

export const selectPrimitivesLoaded = createSelector(
  selectResultsState,
  (state) => state.loaded
);

export const selectAllPrimitives = createSelector(
  selectResultsState,
  (state) => selectAll(state)
);

export const selectPrimitiveEntities = createSelector(
  selectResultsState,
  (state) => selectEntities(state)
);

export const selectAllResults = createSelector(
  JobsSelectors.selectAllJobs,
  AnnotationsSelectors.selectAnnotationEntities,
  selectAllPrimitives,
  (jobs, annotationEntities, primitives) => {
    const results: ResultEntity[] = [];

    jobs.forEach(job => {
      const result: ResultEntity = {
        job,
        annotation: annotationEntities[job.inputs['region_of_interest']] as AnnotationEntity,
        primitives: primitives.filter(p => p.creator_id === job.id)
      };
      if (result.annotation) {
        results.push(result);
      }
    });

    return results;
  }
);

export const selectResultsLoaded = createSelector(
  JobsSelectors.selectJobsLoaded,
  AnnotationsSelectors.selectAnnotationsLoaded,
  selectPrimitivesLoaded,
  (jobsLoaded, annotationsLoaded, primitivesLoaded) => jobsLoaded && annotationsLoaded && primitivesLoaded
);
