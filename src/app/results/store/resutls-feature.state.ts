import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromResults from '@results/store/results/results.reducer';

export const RESULTS_MODULE_FEATURE_KEY = 'resultsModuleFeature';

export const selectResultsFeatureState = createFeatureSelector<State>(
  RESULTS_MODULE_FEATURE_KEY
);

export interface State {
  [fromResults.RESULTS_FEATURE_KEY]: fromResults.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromResults.RESULTS_FEATURE_KEY]: fromResults.reducer,
  })(state,action);
}
