import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { MenuEntity, MenuType } from '@menu/models/menu.models';
import { MenuActions, MenuSelectors } from '@menu/store';
import { ResultEntity } from '@results/store/results/results.models';
import { JobSelector } from '@jobs/store/jobs/jobs.models';
import { ResultsSelectors } from '@results/store';
import { JobsActions, JobsSelectors } from '@jobs/store';
import { Dictionary } from '@ngrx/entity';
import { AnnotationEntity } from '@empaia/slide-viewer';
import { AnnotationsViewerActions } from '@annotations/store';
import { ClassColorEntity, ClassColorsSelectors } from '@classes/store';

@Component({
  selector: 'app-results-container',
  templateUrl: './results-container.component.html',
  styleUrls: ['./results-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsContainerComponent {
  public resultsMenu$: Observable<MenuEntity>;
  public results$: Observable<ResultEntity[]>;
  public resultsLoaded$: Observable<boolean>;
  public jobsSelections$: Observable<Dictionary<JobSelector>>;
  public allJobsSelected$: Observable<boolean>;
  public classColorEntities$: Observable<Dictionary<ClassColorEntity>>;

  constructor(private store: Store) {
    this.resultsMenu$ = this.store.select(MenuSelectors.selectResultsMenu);
    this.results$ = this.store.select(ResultsSelectors.selectAllResults);
    this.resultsLoaded$ = this.store.select(ResultsSelectors.selectResultsLoaded);
    this.jobsSelections$ = this.store.select(JobsSelectors.selectJobSelectionEntities);
    this.allJobsSelected$ = this.store.select(JobsSelectors.selectAllSelectedState);
    this.classColorEntities$ = this.store.select(ClassColorsSelectors.selectAllClassColors);
  }

  onZoomToAnnotation(focus: AnnotationEntity): void {
    this.store.dispatch(AnnotationsViewerActions.zoomToAnnotation({ focus }));
  }

  onJobSelectionChanged(jobSelection: JobSelector): void {
    this.store.dispatch(JobsActions.setJobSelection({ jobSelection }));
  }

  onShowAll(show: boolean): void {
    if (show) {
      this.store.dispatch(JobsActions.showAllJobs());
    } else {
      this.store.dispatch(JobsActions.hideAllJobs());
    }
  }

  onJobInterruption(jobId: string): void {
    this.store.dispatch(JobsActions.stopRunningJob({ jobId }));
  }

  onClickError(menuType: MenuType): void {
    this.store.dispatch(MenuActions.openMenu({ id: menuType }));
  }
}
