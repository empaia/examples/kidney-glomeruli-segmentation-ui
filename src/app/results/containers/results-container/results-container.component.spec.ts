import { ResultsContainerComponent } from './results-container.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents } from 'ng-mocks';
import {
  MenuItemLayoutContainerComponent
} from '@shared/containers/menu-item-layout-container/menu-item-layout-container.component';
import { ResultsLabelComponent } from '@results/components/results-label/results-label.component';
import { ResultsComponent } from '@results/components/results/results.component';
import { provideMockStore } from '@ngrx/store/testing';
import { MenuSelectors } from '@menu/store';
import { ResultsSelectors } from '@results/store';
import { JobsSelectors } from '@jobs/store';

describe('ResultsContainerComponent', () => {
  let spectator: Spectator<ResultsContainerComponent>;
  const createComponent = createComponentFactory({
    component: ResultsContainerComponent,
    declarations: [
      MockComponents(
        MenuItemLayoutContainerComponent,
        ResultsLabelComponent,
        ResultsComponent,
      )
    ],
    providers: [
      provideMockStore({
        selectors: [
          {
            selector: MenuSelectors.selectResultsMenu,
            value: {}
          },
          {
            selector: ResultsSelectors.selectAllResults,
            value: []
          },
          {
            selector: JobsSelectors.selectJobSelectionEntities,
            value: {}
          }
        ]
      })
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
