import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as AnnotationsUiActions from './annotations-ui.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import { ViewportConversionService } from '@annotations/services/viewport-conversion.service';
import { map } from 'rxjs/operators';
import { DrawType, InteractionType } from '@empaia/slide-viewer';


@Injectable()
export class AnnotationsUiEffects {

  convertViewport$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsUiActions.setViewport),
      map(action => action.currentView),
      map(currentView => {
        return {
          viewport: this.viewportConverter.convertFromApi(currentView.extent),
          nppCurrent: currentView.nppCurrent,
        };
      }),
      map(currentView => AnnotationsUiActions.setViewportReady({ currentView }))
    );
  });

  // set allowed interaction types to move and rectangle
  setAllowedInteractionTypes$ = createEffect(() => {
    return this.actions$.pipe(
      // TODO: replay later with action of scope e.g. ScopeActions.setScope
      ofType(SlidesActions.loadSlides),
      map(() => AnnotationsUiActions.setAllowedInteractionTypes({
        allowedDrawTypes: [InteractionType.Move, DrawType.Rectangle]
      }))
    );
  });

  constructor(
    private actions$: Actions,
    private viewportConverter: ViewportConversionService,
  ) {}

}
