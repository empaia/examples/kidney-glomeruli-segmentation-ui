import { createAction, props } from '@ngrx/store';
import { CurrentView as ApiCurrentView, ToolbarInteractionType } from '@empaia/slide-viewer';
import { CurrentView } from '@annotations/store/annotations-ui/annotations-ui.models';


export const setViewport = createAction(
  '[APP/Annotation UI] Set Viewport',
  props<{ currentView: ApiCurrentView }>()
);

export const setViewportReady = createAction(
  '[APP/Annotation UI] Set Viewport Ready',
  props<{ currentView: CurrentView }>()
);

export const setInteractionType = createAction(
  '[APP/Annotation UI] Set Interaction Type',
  props<{ interactionType: ToolbarInteractionType | undefined }>()
);

export const setAllowedInteractionTypes = createAction(
  '[APP/Annotation UI] Set Allowed Interaction Types',
  props<{ allowedDrawTypes: ToolbarInteractionType[] | undefined }>()
);
