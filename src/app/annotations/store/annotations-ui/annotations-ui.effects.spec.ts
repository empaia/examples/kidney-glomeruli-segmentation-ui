import { AnnotationsUiEffects } from './annotations-ui.effects';
import { Observable } from 'rxjs';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { DataPersistence, NxModule } from '@nrwl/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';

describe('AnnotationsUiEffects', () => {
  let action$: Observable<unknown>;
  let spectator: SpectatorService<AnnotationsUiEffects>;
  const createService = createServiceFactory({
    service: AnnotationsUiEffects,
    imports: [
      NxModule.forRoot(),
      HttpClientTestingModule,
    ],
    providers: [
      AnnotationsUiEffects,
      DataPersistence,
      provideMockActions(() => action$),
      provideMockStore()
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
