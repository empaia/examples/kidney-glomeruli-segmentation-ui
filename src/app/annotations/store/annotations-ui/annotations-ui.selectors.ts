import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectAnnotationsFeatureState,
} from '../annotations-feature.state';

export const selectAnnotationsUiState = createSelector(
  selectAnnotationsFeatureState,
  (state: ModuleState) => state.annotationsUi
);

export const selectCurrentView = createSelector(
  selectAnnotationsUiState,
  (state) => state.currentView
);

export const selectInteractionType = createSelector(
  selectAnnotationsUiState,
  (state) => state.interactionType
);

export const selectAllowedInteractionTypes = createSelector(
  selectAnnotationsUiState,
  (state) => state.allowedDrawTypes
);
