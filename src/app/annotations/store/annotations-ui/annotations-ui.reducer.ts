import { createReducer, on } from '@ngrx/store';
import { CurrentView } from '@annotations/store/annotations-ui/annotations-ui.models';
import { ToolbarInteractionType } from '@empaia/slide-viewer';
import * as AnnotationsUiActions from './annotations-ui.actions';


export const ANNOTATIONS_UI_FEATURE_KEY = 'annotationsUi';

export interface State {
  currentView?: CurrentView;
  interactionType?: ToolbarInteractionType;
  allowedDrawTypes?: ToolbarInteractionType[];
}

export const initialState: State = {
  currentView: undefined,
  interactionType: undefined,
  allowedDrawTypes: undefined,
};


export const reducer = createReducer(
  initialState,
  on(AnnotationsUiActions.setViewportReady, (state, { currentView }): State => ({
    ...state,
    currentView,
  })),
  on(AnnotationsUiActions.setInteractionType, (state, { interactionType }): State => ({
    ...state,
    interactionType,
  })),
  on(AnnotationsUiActions.setAllowedInteractionTypes, (state, { allowedDrawTypes }): State => ({
    ...state,
    allowedDrawTypes
  }))
);

