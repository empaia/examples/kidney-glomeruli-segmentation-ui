import { Viewport } from '@api/wbs-api/models/viewport';

export interface CurrentView {
  viewport: Viewport;
  nppCurrent: number;
}
