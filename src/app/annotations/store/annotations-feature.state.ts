import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromAnnotationsViewer from '@annotations/store/annotations-viewer/annotations-viewer.reducer';
import * as fromAnnotationsUi from '@annotations/store/annotations-ui/annotations-ui.reducer';
import * as fromAnnotations from '@annotations/store/annotations/annotations.reducer';

export const ANNOTATIONS_MODULE_FEATURE_KEY = 'annotationsModuleFeature';

export const selectAnnotationsFeatureState = createFeatureSelector<State>(
  ANNOTATIONS_MODULE_FEATURE_KEY
);

export interface State {
  [fromAnnotationsViewer.ANNOTATIONS_VIEWER_FEATURE_KEY]: fromAnnotationsViewer.State;
  [fromAnnotationsUi.ANNOTATIONS_UI_FEATURE_KEY]: fromAnnotationsUi.State;
  [fromAnnotations.ANNOTATION_FEATURE_KEY]: fromAnnotations.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromAnnotationsViewer.ANNOTATIONS_VIEWER_FEATURE_KEY]: fromAnnotationsViewer.reducer,
    [fromAnnotationsUi.ANNOTATIONS_UI_FEATURE_KEY]: fromAnnotationsUi.reducer,
    [fromAnnotations.ANNOTATION_FEATURE_KEY]: fromAnnotations.reducer,
  })(state, action);
}
