import { Observable } from 'rxjs';

import { AnnotationsViewerEffects } from '@annotations/store/annotations-viewer/annotations-viewer.effects';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { DataPersistence, NxModule } from '@nrwl/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';

describe('AnnotationsEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<AnnotationsViewerEffects>;
  const createService = createServiceFactory({
    service: AnnotationsViewerEffects,
    imports: [
      NxModule.forRoot(),
      HttpClientTestingModule,
    ],
    providers: [
      AnnotationsViewerEffects,
      DataPersistence,
      provideMockActions(() => actions$),
      provideMockStore()
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
