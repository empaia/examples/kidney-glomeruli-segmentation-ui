import * as fromAnnotationsViewer from './annotations-viewer.actions';

describe('AnnotationsActions', () => {
  it('should return an action', () => {
    expect(fromAnnotationsViewer.loadAnnotationIds({
      scopeId: '',
      slideId: '',
      jobIds: [],
      currentView: {
        viewport: {
          x: 0,
          y: 0,
          width: 0,
          height: 0,
        },
        nppCurrent: 0
      }
    }).type).toBe('[APP/Annotations-Viewer] Load Annotation Ids');
  });
});
