import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AnnotationConversionService } from '@annotations/services/annotation-conversion.service';
import { ViewportConversionService } from '@annotations/services/viewport-conversion.service';
import * as AnnotationsViewerActions from './annotations-viewer.actions';
import * as AnnotationsUiActions from '@annotations/store/annotations-ui/annotations-ui.actions';
import * as AnnotationsUiSelectors from '@annotations/store/annotations-ui/annotations-ui.selectors';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as SlidesSelectors from '@slides/store/slides/slides.selectors';
import * as JobsActions from '@jobs/store/jobs/jobs.actions';
import * as JobsSelectors from '@jobs/store/jobs/jobs.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as TokenActions from '@token/store/token/token.actions';
import { catchError, filter, map, retryWhen, concatMap } from 'rxjs/operators';
import { ANNOTATION_QUERY_LIMIT } from '@annotations/store/annotations-viewer/annotations-viewer.models';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { AnnotationEntity } from '@empaia/slide-viewer';
import { DataPanelService } from '@api/wbs-api/services/data-panel.service';
import { requestNewToken } from '@empaia/vendor-app-communication-interface';
import { AnnotationApiOutput } from '@annotations/services/annotation-types';
import { of } from 'rxjs';

@Injectable()
export class AnnotationsViewerEffects {

  // clear annotations on slide selection
  clearAnnotationsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        SlidesActions.selectSlide,
        SlidesActions.clearSlides,
      ),
      map(() => AnnotationsViewerActions.clearAnnotations())
    );
  });

  // load annotation ids when the job list was loaded
  startLoadingAnnotationIds$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map(action => action.jobs.map(job => job.id)),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
        this.store.select(AnnotationsUiSelectors.selectCurrentView).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds).pipe(filterNullish())
      ]),
      filter(([, _scopeId, _slideId, _currentView, jobIds]) => !!jobIds.length),
      map(([, scopeId, slideId, currentView, jobIds]) =>
        AnnotationsViewerActions.loadAnnotationIds({
          scopeId,
          slideId,
          jobIds,
          currentView,
        })
      )
    );
  });

  // load new annotation ids when the viewport has changed
  viewportChanged$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsUiActions.setViewportReady),
      map(action => action.currentView),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds)
      ]),
      filter(([_currentView, _scopeId, _slideId, jobIds]) => !!jobIds.length),
      map(([currentView, scopeId, slideId, jobIds]) =>
        AnnotationsViewerActions.loadAnnotationIds({
          scopeId,
          slideId,
          jobIds,
          currentView
        })
      )
    );
  });

  // remove focused annotation after the viewport changed
  removeFocusAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsUiActions.setViewport),
      map(() => AnnotationsViewerActions.zoomToAnnotation({ focus: undefined }))
    );
  });

  // reload annotations when job gets checked or unchecked
  reloadAnnotationIdsOnJobCheck$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.setJobSelection,
        JobsActions.setJobsSelections,
        JobsActions.showAllJobs,
      ),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
        this.store.select(AnnotationsUiSelectors.selectCurrentView).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds)
      ]),
      filter(([, _scopeId, _slideId, _currentView, jobIds]) => !!jobIds.length),
      map(([, scopeId, slideId, currentView, jobIds]) =>
        AnnotationsViewerActions.loadAnnotationIds({
          scopeId,
          slideId,
          currentView,
          jobIds
        })
      )
    );
  });

  loadAnnotationIds$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.loadAnnotationIds),
      concatMap(action =>
        this.dataPanelService.scopeIdAnnotationsQueryViewerPut({
          scope_id: action.scopeId,
          body: {
            references: [
              action.slideId
            ],
            jobs: action.jobIds.length ? action.jobIds : [null],
            viewport: action.currentView.viewport,
            npp_viewing: this.viewportConverter.calculateNppViewRange(action.currentView.nppCurrent),
            class_values: action.classValues,
            types: action.annotationTypes?.map(this.annotationConverter.convertAnnotationTypeToApi)
          }
        }).pipe(
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          map(results => AnnotationsViewerActions.loadAnnotationIdsSuccess({
            annotationIds: results.annotations,
            centroids: results.low_npp_centroids
          })),
          catchError(error => of(AnnotationsViewerActions.loadAnnotationIdsFailure({ error })))
        )
      )
    );
  });

  prepareLoadAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.loadAnnotations),
      map(action => action.annotationIds),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds),
      ]),
      filter(([annotationIds, _scopeId, jobIds]) =>
        !!annotationIds.length
        && !!jobIds.length
      ),
      map(([annotationIds, scopeId, jobIds]) =>
        AnnotationsViewerActions.loadAnnotationsReady({
          scopeId,
          annotationIds,
          jobIds,
          withClasses: true,
          skip: 0,
        })
      )
    );
  });

  loadAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.loadAnnotationsReady),
      concatMap(action =>
        this.dataPanelService.scopeIdAnnotationsQueryPut({
          scope_id: action.scopeId,
          with_classes: action.withClasses,
          skip: action.skip,
          limit: ANNOTATION_QUERY_LIMIT,
          body: {
            annotations: action.annotationIds,
            jobs: action.jobIds?.length ? action.jobIds : [null]
          }
        }).pipe(
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          map(result => result.items.map(a => this.annotationConverter.fromApiType(a) as AnnotationEntity)),
          map(annotations => AnnotationsViewerActions.loadAnnotationsSuccess({ annotations })),
          catchError(error => of(AnnotationsViewerActions.loadAnnotationsFailure({ error })))
        )
      )
    );
  });

  prepareCreateAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.createAnnotation),
      map(action => action.annotation),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish())
      ]),
      map(([annotation, scopeId, slideId]) => AnnotationsViewerActions.createAnnotationReady({
        annotation,
        scopeId,
        slideId,
      }))
    );
  });

  createAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.createAnnotationReady),
      concatMap(action =>
        this.dataPanelService.scopeIdAnnotationsPost({
          scope_id: action.scopeId,
          is_roi: true,
          body: this.annotationConverter.toApiPostType(
            action.annotation,
            action.slideId,
            action.scopeId,
          )
        }).pipe(
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          map(result => this.annotationConverter.fromApiType(result as AnnotationApiOutput) as AnnotationEntity),
          map(annotation => AnnotationsViewerActions.createAnnotationSuccess({ annotation })),
          catchError(error => of(AnnotationsViewerActions.createAnnotationFailure({ error })))
        )
      )
    );
  });

  // delete the current annotation when something goes wrong at setting the job inputs
  removeAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setJobInputFailure),
      map(action => AnnotationsViewerActions.deleteAnnotation({ ...action }))
    );
  });

  deleteAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.deleteAnnotation),
      concatMap(action =>
        this.dataPanelService.scopeIdAnnotationsAnnotationIdDelete({
          scope_id: action.scopeId,
          annotation_id: action.annotationId
        }).pipe(
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          map(result => result.id),
          map(annotationId => AnnotationsViewerActions.deleteAnnotationSuccess({ annotationId })),
          catchError(error => of(AnnotationsViewerActions.deleteAnnotationFailure({ error })))
        )
      )
    );
  });

  // add focused annotation to slide-viewer cache
  addAnnotationOnFocus$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.zoomToAnnotation),
      map(action => action.focus),
      filterNullish(),
      map(annotation => AnnotationsViewerActions.addAnnotation({ annotation }))
    );
  });

  prepareLoadHiddenAnnotationIds$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.setJobSelection,
        JobsActions.setJobsSelections,
        JobsActions.hideAllJobs,
      ),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
        this.store.select(AnnotationsUiSelectors.selectCurrentView).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllUncheckedJobIds).pipe(filterNullish()),
      ]),
      filter(([, _scopeId, _slideId, _currentView, jobIds]) => !!jobIds.length),
      map(([, scopeId, slideId, currentView, jobIds]) =>
        AnnotationsViewerActions.loadHiddenAnnotationIds({
          scopeId,
          slideId,
          currentView,
          jobIds
        })
      )
    );
  });

  loadHiddenAnnotationIds$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.loadHiddenAnnotationIds),
      concatMap(action =>
        this.dataPanelService.scopeIdAnnotationsQueryViewerPut({
          scope_id: action.scopeId,
          body: {
            references: [
              action.slideId
            ],
            jobs: action.jobIds.length ? action.jobIds : [null],
            viewport: action.currentView.viewport,
            npp_viewing: this.viewportConverter.calculateNppViewRange(action.currentView.nppCurrent),
            class_values: action.classValues,
            types: action.annotationTypes?.map(this.annotationConverter.convertAnnotationTypeToApi)
          }
        }).pipe(
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          map(results => results.annotations),
          map(hidden => AnnotationsViewerActions.loadHiddenAnnotationIdsSuccess({ hidden })),
          catchError(error => of(AnnotationsViewerActions.loadHiddenAnnotationIdsFailure({ error })))
        )
      )
    );
  });

  // clear centroids on hide or result selection when no job is checked
  clearCentroids$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.setJobSelection,
        JobsActions.setJobsSelections,
        JobsActions.hideAllJobs,
      ),
      concatLatestFrom(() =>
        this.store.select(JobsSelectors.selectAllCheckedJobIds)
      ),
      filter(([, jobIds]) => !jobIds.length),
      map(() => AnnotationsViewerActions.clearCentroids())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataPanelService: DataPanelService,
    private readonly annotationConverter: AnnotationConversionService,
    private readonly viewportConverter: ViewportConversionService,
  ) {}

}
