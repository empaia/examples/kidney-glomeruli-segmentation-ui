import { createReducer, on } from '@ngrx/store';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { AnnotationEntity } from '@empaia/slide-viewer';
import { HttpError } from '@menu/models/ui.model';
import * as AnnotationsActions from '@annotations/store/annotations/annotations.actions';


export const ANNOTATION_FEATURE_KEY = 'annotations';

export interface State extends EntityState<AnnotationEntity> {
  loaded: boolean;
  error?: HttpError | undefined;
}

export const annotationAdapter = createEntityAdapter<AnnotationEntity>();

export const initialState: State = annotationAdapter.getInitialState({
  loaded: true,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(AnnotationsActions.loadInputAnnotations, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(AnnotationsActions.loadInputAnnotationsSuccess, (state, { annotations }): State =>
    annotationAdapter.setAll(annotations, {
      ...state,
      loaded: true,
    })
  ),
  on(AnnotationsActions.loadInputAnnotationsFailure, (state, { error }): State => ({
    ...state,
    error,
    loaded: true,
  })),
  on(AnnotationsActions.clearAnnotations, (state): State =>
    annotationAdapter.removeAll({
      ...state,
      ...initialState,
    })
  ),
);
