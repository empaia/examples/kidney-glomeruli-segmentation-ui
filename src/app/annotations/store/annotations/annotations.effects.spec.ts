import { Observable } from 'rxjs';

import { AnnotationsEffects } from '@annotations/store/annotations/annotations.effects';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { DataPersistence, NxModule } from '@nrwl/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';

describe('AnnotationsEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<AnnotationsEffects>;
  const createService = createServiceFactory({
    service: AnnotationsEffects,
    imports: [
      NxModule.forRoot(),
      HttpClientTestingModule,
    ],
    providers: [
      AnnotationsEffects,
      DataPersistence,
      provideMockActions(() => actions$),
      provideMockStore()
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
