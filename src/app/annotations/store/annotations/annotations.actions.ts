import { createAction, props } from '@ngrx/store';
import { AnnotationEntity } from '@empaia/slide-viewer';
import { HttpError } from '@menu/models/ui.model';

export const loadInputAnnotations = createAction(
  '[APP/Annotations] Load Input Annotations',
  props<{
    scopeId: string,
    withClasses: boolean,
    skip: number,
    limit: number,
    slideId: string,
    jobIds: string[],
  }>()
);

export const loadInputAnnotationsSuccess = createAction(
  '[APP/Annotations] Load Input Annotations Success',
  props<{ annotations: AnnotationEntity[] }>()
);

export const loadInputAnnotationsFailure = createAction(
  '[APP/Annotations] Load Input Annotations Failure',
  props<{ error: HttpError }>()
);

export const clearAnnotations = createAction(
  '[APP/Annotations] Clear Annotations',
);
