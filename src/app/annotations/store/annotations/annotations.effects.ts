import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { DataPanelService } from '@api/wbs-api/services/data-panel.service';
import { AnnotationConversionService } from '@annotations/services/annotation-conversion.service';
import * as AnnotationsActions from '@annotations/store/annotations/annotations.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as JobsActions from '@jobs/store/jobs/jobs.actions';
import * as SlidesSelectors from '@slides/store/slides/slides.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as TokenActions from '@token/store/token/token.actions';
import { filter, map, retryWhen, catchError, concatMap } from 'rxjs/operators';
import { AnnotationEntity } from '@empaia/slide-viewer';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { INPUT_ANNOTATION_LIMIT } from '@annotations/store/annotations/annotations.models';
import { requestNewToken } from '@empaia/vendor-app-communication-interface';
import { of } from 'rxjs';



@Injectable()
export class AnnotationsEffects {

  // clear annotations on slide selection
  clearAnnotationsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        SlidesActions.selectSlide,
        SlidesActions.clearSlides,
      ),
      map(() => AnnotationsActions.clearAnnotations())
    );
  });

  // load annotations when job list was fetched
  loadAnnotationsAfterJobs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map(action => action.jobs.map(j => j.id)),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
      ]),
      filter(([jobIds]) => !!jobIds && !!jobIds.length),
      map(([jobIds, scopeId, slideId]) =>
        AnnotationsActions.loadInputAnnotations({
          scopeId,
          slideId,
          jobIds,
          withClasses: true,
          skip: 0,
          limit: INPUT_ANNOTATION_LIMIT,
        })
      )
    );
  });

  loadAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsActions.loadInputAnnotations),
      concatMap(action =>
        this.dataPanelService.scopeIdAnnotationsQueryPut({
          scope_id: action.scopeId,
          with_classes: action.withClasses,
          skip: action.skip,
          limit: action.limit,
          body: {
            creators: [
              action.scopeId
            ],
            references: [
              action.slideId
            ],
            jobs: action.jobIds?.length ? action.jobIds : [null],
          }
        }).pipe(
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          map(result => result.items.map(a => this.annotationConversion.fromApiType(a) as AnnotationEntity)),
          map(annotations => AnnotationsActions.loadInputAnnotationsSuccess({ annotations })),
          catchError(error => of(AnnotationsActions.loadInputAnnotationsFailure({ error })))
        )
      )
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataPanelService: DataPanelService,
    private readonly annotationConversion: AnnotationConversionService,
  ) {}

}
