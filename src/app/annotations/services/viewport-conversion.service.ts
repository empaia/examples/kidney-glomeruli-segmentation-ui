import { Injectable } from '@angular/core';
import { Extent } from 'ol/extent';
import { Viewport } from '@api/wbs-api/models/viewport';
import { NPP_MAX_RANGE_FACTOR, NPP_MIN_RANGE_FACTOR } from '@annotations/store/annotations-viewer/annotations-viewer.models';

@Injectable({
  providedIn: 'root'
})
export class ViewportConversionService {

  public convertFromApi(extent: Extent): Viewport {
    // make sure that all coordinates have a positive value and
    // round coordinates to an integer value, because the backend
    // doesn't accept floating points
    const viewportArray = extent.map(elem => elem >= 0 ? Math.round(elem) : 0);
    const width = viewportArray[2] - viewportArray[0];
    const height = viewportArray[3] - viewportArray[1];
    return {
      x: viewportArray[0],
      y: viewportArray[1],
      // get sure width and height are higher than zero
      width: width > 0 ? width : 1,
      height: height > 0 ? height : 1,
    };
  }

  public calculateNppViewRange(nppCurrent: number | undefined): number[] | undefined {
    if (!nppCurrent) {
      return undefined;
    }

    const min = NPP_MIN_RANGE_FACTOR * nppCurrent;
    const max = NPP_MAX_RANGE_FACTOR * nppCurrent;
    return [min, max];
  }
}
