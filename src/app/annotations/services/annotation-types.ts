import {
  AnnotationCircle,
  AnnotationRectangle,
  AnnotationPolygon,
  AnnotationPoint
} from '@empaia/slide-viewer';

import {
  // get types
  PointAnnotation,
  LineAnnotation,
  ArrowAnnotation,
  CircleAnnotation,
  RectangleAnnotation,
  PolygonAnnotation,
  //post types
  PostArrowAnnotation,
  PostCircleAnnotation,
  PostLineAnnotation,
  PostRectangleAnnotation,
  PostPolygonAnnotation,
  PostPointAnnotation
} from '@api/wbs-api/models';

export type AnnotationApiOutput =
  | PointAnnotation
  | LineAnnotation
  | ArrowAnnotation
  | CircleAnnotation
  | RectangleAnnotation
  | PolygonAnnotation
  ;

export type AnnotationViewerInput =
  | AnnotationCircle
  | AnnotationRectangle
  | AnnotationPolygon
  | AnnotationPoint
  ;

export type AnnotationApiPostType =
  | PostArrowAnnotation
  | PostCircleAnnotation
  | PostLineAnnotation
  | PostRectangleAnnotation
  | PostPolygonAnnotation
  | PostPointAnnotation
  ;
