import { AnnotationConversionService } from './annotation-conversion.service';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';

describe('AnnotationConversionService', () => {
  let spectator: SpectatorService<AnnotationConversionService>;
  const createService = createServiceFactory({
    service: AnnotationConversionService,
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const service = spectator.service;
    expect(service).toBeTruthy();
  });
});
