import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { ANNOTATIONS_MODULE_FEATURE_KEY, reducers } from '@annotations/store/annotations-feature.state';
import { EffectsModule } from '@ngrx/effects';
import {
  AnnotationsEffects,
  AnnotationsUiEffects,
  AnnotationsViewerEffects
} from '@annotations/store';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      ANNOTATIONS_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      AnnotationsEffects,
      AnnotationsUiEffects,
      AnnotationsViewerEffects,
    ]),
  ]
})
export class AnnotationsModule { }
