import { ScopeEffects } from '@scope/store/scope/scope.effects';
import { Observable } from 'rxjs';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { DataPersistence, NxModule } from '@nrwl/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';

describe('ScopeEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<ScopeEffects>;
  const createService = createServiceFactory({
    service: ScopeEffects,
    imports: [
      NxModule.forRoot(),
      HttpClientTestingModule
    ],
    providers: [
      ScopeEffects,
      DataPersistence,
      provideMockActions(() => actions$),
      provideMockStore()
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
