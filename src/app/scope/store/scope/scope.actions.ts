import { createAction, props } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { ExtendedScope } from '@api/wbs-api/models/extended-scope';

export const setScope = createAction(
  '[APP/Scope] Set Scope',
  props<{ scopeId: string }>()
);

export const loadExtendedScope = createAction(
  '[APP/Scope] Load Extended Scope',
  props<{ scopeId: string }>()
);

export const loadExtendedScopeSuccess = createAction(
  '[APP/Scope] Load Extended Scope Success',
  props<{ extendedScope: ExtendedScope }>()
);

export const loadExtendedScopeFailure = createAction(
  '[APP/Scope] Load Extended Scope Failure',
  props<{ error: HttpErrorResponse }>()
);
