import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectScopeFeatureState,
} from '../scope-feature.state';

export const selectScopeState = createSelector(
  selectScopeFeatureState,
  (state: ModuleState) => state.scope
);

export const selectScopeId = createSelector(
  selectScopeState,
  (state) => state.scopeId
);

export const selectExtendedScope = createSelector(
  selectScopeState,
  (state) => state.extendedScope
);
