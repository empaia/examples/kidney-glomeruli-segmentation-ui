import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './containers/app/app.component';
import { LoadingIndicatorComponent } from './components/loading-indicator/loading-indicator.component';
import { MaterialModule } from '@material/material.module';

import { SlideViewerModule } from '@empaia/slide-viewer';
import { MenuModule } from '@menu/menu.module';

@NgModule({
  declarations: [
    AppComponent,
    LoadingIndicatorComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    MenuModule,
    SlideViewerModule,
  ]
})
export class CoreModule { }
