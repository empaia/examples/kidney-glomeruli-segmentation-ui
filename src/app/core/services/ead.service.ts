/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EadService {
  public getInputKeys(ead: any): string[] {
    return Object.keys(ead['inputs']);
  }
}
