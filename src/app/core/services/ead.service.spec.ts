import { TestBed } from '@angular/core/testing';

import { EadService } from './ead.service';

describe('EadService', () => {
  let service: EadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
