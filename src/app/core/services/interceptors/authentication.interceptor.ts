import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AccessTokenService } from '@token/services/access-token.service';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {

  constructor(private accessTokenService: AccessTokenService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const authReq = request.clone({
      setHeaders: {
        [this.accessTokenService.headerKey]: this.accessTokenService.accessToken,
      }
    });

    return next.handle(authReq);
  }
}
