import { createAction, props } from '@ngrx/store';
import { Token } from '@empaia/vendor-app-communication-interface';

export const setAccessToken = createAction(
  '[APP/Token] Set Access Token',
  props<{ token: Token }>()
);

export const requestNewToken = createAction(
  '[APP/Token] Request New Token',
);
