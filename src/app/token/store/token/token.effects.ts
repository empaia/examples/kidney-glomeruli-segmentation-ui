import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AccessTokenService } from '@token/services/access-token.service';
import * as TokenActions from './token.actions';
import { requestNewToken } from '@empaia/vendor-app-communication-interface';
import { map } from 'rxjs/operators';



@Injectable()
export class TokenEffects {

  setAccessToken$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TokenActions.setAccessToken),
      map(action => action.token),
      map(token => this.accessTokenService.accessToken = token.value)
    );
  }, { dispatch: false });

  requestNewToken$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TokenActions.requestNewToken),
      map(() => requestNewToken())
    );
  }, { dispatch: false });

  constructor(
    private readonly actions$: Actions,
    private readonly accessTokenService: AccessTokenService,
  ) {}

}
