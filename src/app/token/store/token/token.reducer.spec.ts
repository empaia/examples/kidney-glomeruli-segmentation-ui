/* eslint-disable @typescript-eslint/no-explicit-any */
import { reducer, initialState } from './token.reducer';

describe('Token Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
