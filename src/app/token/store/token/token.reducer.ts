import { createReducer, on } from '@ngrx/store';
import { Token } from '@empaia/vendor-app-communication-interface';
import * as TokenActions from './token.actions';


export const TOKEN_FEATURE_KEY = 'token';

export interface State {
  token: Token | undefined;
}

export const initialState: State = {
  token: undefined,
};


export const reducer = createReducer(
  initialState,
  on(TokenActions.setAccessToken, (state, { token }): State => ({
    ...state,
    token,
  })),
);

