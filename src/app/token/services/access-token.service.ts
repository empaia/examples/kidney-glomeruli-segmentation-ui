import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AccessTokenService {
  private _accessToken!: string;
  private _headerKey = 'Authorization';
  private _tokenPreamble = 'Bearer ';

  public get accessToken() {
    return this._accessToken;
  }

  public set accessToken(val: string) {
    this._accessToken = this._tokenPreamble + val;
  }

  public get headerKey() {
    return this._headerKey;
  }

  public getAccessToken(): string {
    return this._accessToken;
  }
}
