import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { TOKEN_MODULE_FEATURE_KEY, reducers } from '@token/store/token-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { TokenEffects } from '@token/store';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      TOKEN_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      TokenEffects
    ])
  ]
})
export class TokenModule { }
