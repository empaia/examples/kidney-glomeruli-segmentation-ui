import { CopyToClipboardDirective } from './copy-to-clipboard.directive';
import { createDirectiveFactory, SpectatorDirective } from '@ngneat/spectator';
import { MatSnackBarModule } from '@angular/material/snack-bar';

describe('CopyToClipboardDirective', () => {
  let spectator: SpectatorDirective<CopyToClipboardDirective<unknown>>;
  const createDirective = createDirectiveFactory({
    directive: CopyToClipboardDirective,
    imports: [
      MatSnackBarModule,
    ],
  });

  beforeEach(() => spectator = createDirective(`<div [appCopyToClipboard]="{id: TEST-ID}"></div>`));

  it('should create an instance', () => {
    expect(spectator.directive).toBeTruthy();
  });
});
