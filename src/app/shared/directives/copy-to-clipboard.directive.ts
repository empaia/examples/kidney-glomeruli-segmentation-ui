import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Directive({
  selector: '[appCopyToClipboard]'
})
export class CopyToClipboardDirective<T> {

  public key: string | undefined;

  @Input('appCopyToClipboard')
  public content!: T;

  @Output()
  public copied: EventEmitter<string> = new EventEmitter<string>();

  constructor(private _snackBar: MatSnackBar) {

  }

  @HostListener('click', ['$event'])
  public onClick($event: MouseEvent): void {
    if(this.key && this.content) {
      $event.preventDefault();

      if(this.key === 'i') {
        // user requests id of selected item
        const id = this.content['id'] ?? (this.content['item'].id ?? undefined);
        if (id) {
          navigator.clipboard.writeText(id).then().catch(e => console.error(e));
          this.notify('ID');
        }
      } else if (this.key === 'j') {
        // user requests selected item as json string
        navigator.clipboard.writeText(
          JSON.stringify(this.content, null, 2)
        ).then().catch(e => console.error(e));
        this.notify('JSON-Object');
      }

      $event.stopPropagation();
      $event.stopImmediatePropagation();
    }
  }

  private notify(text: string): void {
    this._snackBar.open(`${text} was copied to clipboard`, 'Ok', {
      duration: 500
    });
  }

  @HostListener('document:keydown', ['$event'])
  public keyDown($event: KeyboardEvent) {
    this.key = $event.key;
  }

  @HostListener('document:keyup', ['$event'])
  public keyUp(_$event: KeyboardEvent) {
    this.key = undefined;
  }

}
