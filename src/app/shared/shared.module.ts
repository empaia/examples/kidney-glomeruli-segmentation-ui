import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuItemLayoutContainerComponent } from './containers/menu-item-layout-container/menu-item-layout-container.component';
import { MenuLabelComponent } from './components/menu-label/menu-label.component';
import { LoadingComponent } from './components/loading/loading.component';
import { MaterialModule } from '@material/material.module';
import { StringSlicerPipe } from './pipes/string-slicer.pipe';
import { MissingSelectionErrorComponent } from './components/missing-selection-error/missing-selection-error.component';
import { LocalSafeUrlPipe } from './pipes/local-safe-url.pipe';
import { DictParserPipe } from './pipes/dict-parser.pipe';
import { MenuButtonComponent } from './components/menu-button/menu-button.component';
import { JobStatusComponent } from './components/job-status/job-status.component';
import { IconCheckboxComponent } from './components/icon-checkbox/icon-checkbox.component';
import { CopyToClipboardDirective } from './directives/copy-to-clipboard.directive';
import { CloseButtonComponent } from './components/close-button/close-button.component';

const COMPONENTS = [
  MenuItemLayoutContainerComponent,
  MenuLabelComponent,
  LoadingComponent,
  MissingSelectionErrorComponent,
  MenuButtonComponent,
  JobStatusComponent,
  IconCheckboxComponent,
  CloseButtonComponent,
];

const PIPES = [
  StringSlicerPipe,
  LocalSafeUrlPipe,
  DictParserPipe,
];

const DIRECTIVES = [
  CopyToClipboardDirective,
];

@NgModule({
  declarations: [
    COMPONENTS,
    PIPES,
    DIRECTIVES,
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ],
  exports: [
    COMPONENTS,
    PIPES,
    DIRECTIVES,
  ]
})
export class SharedModule { }
