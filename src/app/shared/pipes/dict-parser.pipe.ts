import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dictParser'
})
export class DictParserPipe implements PipeTransform {

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  transform(dict: any, lang: string = 'EN'): string {
    if (!dict) {
      return '-';
    }

    const values = [];
    for (const key of Object.keys(dict)) {
      const v = dict[key][lang];
      if (v) {
        values.push(v);
      }
    }

    return values.length === 0? '-' : values.join(', ');
  }

}
