import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { JobStatus } from '@api/wbs-api/models/job-status';

@Component({
  selector: 'app-job-status',
  templateUrl: './job-status.component.html',
  styleUrls: ['./job-status.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JobStatusComponent {
  public readonly JOB_STATUS = JobStatus;
  @Input() jobStatus!: JobStatus;
  @Input() jobIndicatorClass!: string;
}
