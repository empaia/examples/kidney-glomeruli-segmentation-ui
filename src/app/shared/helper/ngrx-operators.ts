import { EntityAdapter, EntityState } from '@ngrx/entity';

// an entity must minimal consists of an id to use
// this functions
interface Entity {
  id: string | number;
}

function upsertManyWithoutUpdate<T extends Entity>(adapter: EntityAdapter<T>, entries: T[], state: EntityState<T>): EntityState<T> {
  const present = adapter.getSelectors().selectAll(state);
  const newEntries = entries.filter(entry => !present.map(e => e.id).includes(entry.id));
  const concat = present.concat(newEntries);
  return adapter.setAll(concat, { ...state });
}

export {
  upsertManyWithoutUpdate
};
