import { WbsUrlEffects } from '@wbsUrl/store/wbs-url/wbs-url.effects';
import { Observable } from 'rxjs';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { WbsUrlService } from '@wbsUrl/services/wbs-url.service';
import { provideMockActions } from '@ngrx/effects/testing';

describe('WbsUrlEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<WbsUrlEffects>;
  const createService = createServiceFactory({
    service: WbsUrlEffects,
    providers: [
      WbsUrlEffects,
      WbsUrlService,
      provideMockActions(() => actions$)
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
