import { createAction, props } from '@ngrx/store';
import { WbsUrl } from '@empaia/vendor-app-communication-interface';

export const setWbsUrl = createAction(
  '[APP/WbsUrl] Set Wbs Url',
  props<{ wbsUrl: WbsUrl }>()
);
