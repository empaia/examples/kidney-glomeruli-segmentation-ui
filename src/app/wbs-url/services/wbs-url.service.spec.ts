import { TestBed } from '@angular/core/testing';

import { WbsUrlService } from './wbs-url.service';

describe('WbsUrlService', () => {
  let service: WbsUrlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WbsUrlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
