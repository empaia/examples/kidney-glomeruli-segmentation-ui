import { Injectable } from '@angular/core';
import { ApiConfiguration } from '@api/wbs-api/api-configuration';

@Injectable({
  providedIn: 'root'
})
export class WbsUrlService {
  private _wbsUrl!: string;
  private _wbsUrlExtension = '/v2/scopes';

  constructor(private apiConfig: ApiConfiguration) { }

  public get wbsUrl() {
    return this._wbsUrl;
  }

  public set wbsUrl(val: string) {
    this._wbsUrl = val;
    this.setRootUrl();
  }

  public get wbsUrlExtension() {
    return this._wbsUrlExtension;
  }

  public get wbsUrlComplete() {
    return this._wbsUrl + this._wbsUrlExtension;
  }

  private setRootUrl(): void {
    this.apiConfig.rootUrl = this.wbsUrlComplete;
  }
}
