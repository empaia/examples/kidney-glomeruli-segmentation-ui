import { createAction, props } from '@ngrx/store';
import { Job } from '@api/wbs-api/models/job';
import { HttpError } from '@menu/models/ui.model';
import { JobCreatorType } from '@api/wbs-api/models/job-creator-type';
import { JobSelector } from '@jobs/store/jobs/jobs.models';

export const loadJobs = createAction(
  '[APP/Jobs] Load Jobs',
  props<{ scopeId: string }>()
);

export const loadJobsSuccess = createAction(
  '[APP/Jobs] Load Jobs Success',
  props<{ jobs: Job[] }>()
);

export const loadJobsFailure = createAction(
  '[APP/Jobs] Load Jobs Failure',
  props<{ error: HttpError }>()
);

export const startJobsPolling = createAction(
  '[APP/Jobs] Start Jobs Polling'
);

export const stopJobsPolling = createAction(
  '[APP/Jobs] Stop Jobs Polling'
);

export const createJob = createAction(
  '[APP/Jobs] Create Job',
  props<{
    scopeId: string,
    appId: string,
    creatorId: string,
    creatorType: JobCreatorType,
    annotationId: string
  }>()
);

export const setWsiJobInput = createAction(
  '[APP/Jobs] Set Wsi Job Input',
  props<{
    scopeId: string,
    jobId: string,
    annotationId: string,
  }>()
);

export const setWsiJobInputReady = createAction(
  '[APP/Jobs] Set Wsi Job Input Ready',
  props<{
    scopeId: string,
    jobId: string,
    slideInputKey: string,
    slideId: string,
    annotationInputKey: string,
    annotationId: string,
  }>()
);

export const setAnnotationJobInput = createAction(
  '[APP/Jobs] Set Annotation Job Input',
  props<{
    scopeId: string,
    jobId: string,
    annotationInputKey: string,
    annotationId: string,
  }>()
);

export const runJob = createAction(
  '[App/Jobs] Run Job',
  props<{
    scopeId: string,
    jobId: string,
    annotationId: string,
  }>()
);

export const createJobSuccess = createAction(
  '[APP/Jobs] Create Job Success',
  props<{ job: Job }>()
);

export const createJobFailure = createAction(
  '[APP/Jobs] Create Job Failure',
  props<{ error: HttpError }>()
);

export const setJobInputFailure = createAction(
  '[APP/Jobs] Set Job Input Failure',
  props<{
    error: HttpError,
    scopeId: string,
    jobId: string,
    annotationId: string,
  }>()
);

export const deleteJob = createAction(
  '[APP/Jobs] Delete Job',
  props<{ scopeId: string, jobId: string }>()
);

export const deleteJobSuccess = createAction(
  '[APP/Jobs] Delete Job Success',
  props<{ jobId: string }>()
);

export const deleteJobFailure = createAction(
  '[APP/Jobs] Delete Job Failure',
  props<{ error: HttpError }>()
);

export const setJobSelection = createAction(
  '[APP/Jobs] Set Job Selection',
  props<{ jobSelection: JobSelector }>()
);

export const setJobsSelections = createAction(
  '[APP/Jobs] Set Jobs Selections',
  props<{ jobsSelections: JobSelector[] }>()
);

export const showAllJobs = createAction(
  '[APP/Jobs] Show All Jobs',
);

export const hideAllJobs = createAction(
  '[APP/Jobs] Hide All Jobs',
);

export const clearJobs = createAction(
  '[APP/Jobs] Clear Jobs',
);

export const stopRunningJob = createAction(
  '[Jobs] Stop Running Job',
  props<{ jobId: string }>()
);

export const stopRunningJobReady = createAction(
  '[Jobs] Stop Running Job Ready',
  props<{ scopeId: string, jobId: string }>()
);

export const stopRunningJobSuccess = createAction(
  '[Jobs] Stop Running Job Success',
  props<{ done: boolean }>()
);

export const stopRunningJobFailure = createAction(
  '[Jobs] Stop Running Job Failure',
  props<{ error: HttpError }>()
);
