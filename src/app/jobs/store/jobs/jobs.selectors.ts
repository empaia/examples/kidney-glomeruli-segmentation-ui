import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectJobsFeatureState,
} from '../jobs-feature.state';
import { jobAdapter } from '@jobs/store/jobs/jobs.reducer';
import { jobSelectorAdapter } from '@jobs/store/jobs/jobs.reducer';

const {
  selectIds,
  selectAll,
} = jobAdapter.getSelectors();

export const selectJobState = createSelector(
  selectJobsFeatureState,
  (state: ModuleState) => state.jobs
);

export const selectJobsLoaded = createSelector(
  selectJobState,
  (state) => state.loaded
);

export const selectAllJobs = createSelector(
  selectJobState,
  (state) => selectAll(state)
);

export const selectAllJobIds = createSelector(
  selectJobState,
  (state) => selectIds(state) as string[]
);

export const selectAllSelectedState = createSelector(
  selectJobState,
  (state) => state.allSelected
);

export const selectAllJobsSelections = createSelector(
  selectJobState,
  (state) => jobSelectorAdapter.getSelectors().selectAll(state.selectedJobs)
);

export const selectJobSelectionEntities = createSelector(
  selectJobState,
  (state) => jobSelectorAdapter.getSelectors().selectEntities(state.selectedJobs)
);

export const selectAllCheckedJobIds = createSelector(
  selectAllJobsSelections,
  (jobsSelections) => jobsSelections.filter(j => !!j.checked).map(j => j.id)
);

export const selectAllUncheckedJobIds = createSelector(
  selectAllJobsSelections,
  (jobsSelections) => jobsSelections.filter(j => !j.checked).map(j => j.id)
);
