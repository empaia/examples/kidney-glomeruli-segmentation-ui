import { Observable } from 'rxjs';

import { JobsEffects } from './jobs.effects';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { DataPersistence, NxModule } from '@nrwl/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';

describe('JobsEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<JobsEffects>;
  const createService = createServiceFactory({
    service: JobsEffects,
    imports: [
      NxModule.forRoot(),
      HttpClientTestingModule,
    ],
    providers: [
      JobsEffects,
      DataPersistence,
      provideMockActions(() => actions$),
      provideMockStore(),
      { provide: 'EXAMINATION_ID', useValue: '' },
      { provide: 'APP_ID', useValue: '' },
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
