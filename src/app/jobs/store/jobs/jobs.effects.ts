import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as JobsActions from './jobs.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as SlidesSelectors from '@slides/store/slides/slides.selectors';
import * as AnnotationsViewerActions from '@annotations/store/annotations-viewer/annotations-viewer.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as TokenActions from '@token/store/token/token.actions';
import { filter, map, retryWhen, switchMap, takeUntil, catchError, concatMap } from 'rxjs/operators';
import { timer } from 'rxjs';
import { isJobRunning, JOB_POLLING_PERIOD } from '@jobs/store/jobs/jobs.models';
import { JobsPanelService } from '@api/wbs-api/services/jobs-panel.service';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { JobCreatorType } from '@api/wbs-api/models/job-creator-type';
import { EadService } from '@core/services/ead.service';
import { requestNewToken } from '@empaia/vendor-app-communication-interface';
import { of } from 'rxjs';



@Injectable()
export class JobsEffects {

  // clear jobs on slide selection
  clearJobsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        SlidesActions.selectSlide,
        SlidesActions.clearSlides,
      ),
      map(() => JobsActions.clearJobs())
    );
  });

  // create job when annotation was created
  prepareCreateJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.createAnnotationSuccess),
      map(action => action.annotation.id),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(filterNullish())
      ]),
      map(([annotationId, scopeId, scopeExtended]) => JobsActions.createJob({
        scopeId,
        appId: scopeExtended.app_id,
        creatorId: scopeId,
        creatorType: JobCreatorType.Scope,
        annotationId,
      }))
    );
  });

  loadJobs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobs),
      switchMap(action =>
        this.jobsPanelService.scopeIdJobsGet({
          scope_id: action.scopeId
        }).pipe(
          // retry to load the selected slide after the token is expired
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          map(items => items.items),
          map(jobs => JobsActions.loadJobsSuccess({ jobs })),
          catchError(error => of(JobsActions.loadJobsFailure({ error })))
        )
      )
    )
  });

  jobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.startJobsPolling),
      switchMap(() =>
        timer(0, JOB_POLLING_PERIOD).pipe(
          takeUntil(this.actions$.pipe(ofType(JobsActions.stopJobsPolling))),
          concatLatestFrom(() =>
            this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())
          ),
          map(([, scopeId]) => scopeId),
          map(scopeId => JobsActions.loadJobs({ scopeId }))
        )
      )
    );
  });

  stopJobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map(action => action.jobs),
      filter(jobs => !jobs.find(isJobRunning)),
      map(() => JobsActions.stopJobsPolling())
    );
  });

  startJobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.createJobSuccess,
        SlidesActions.selectSlide,
      ),
      map(() => JobsActions.startJobsPolling())
    );
  });

  createJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.createJob),
      concatMap(action => this.jobsPanelService.scopeIdJobsPost({
          scope_id: action.scopeId,
          body: {
            app_id: action.appId,
            creator_id: action.creatorId,
            creator_type: action.creatorType,
          }
        }).pipe(
          // retry to load the selected slide after the token is expired
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          map(job => job.id),
          map(jobId => JobsActions.setWsiJobInput({
            scopeId: action.scopeId,
            jobId,
            annotationId: action.annotationId,
          })),
          catchError(error => of(JobsActions.createJobFailure({ error })))
        )
      )
    )
  });

  prepareWsiJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setWsiJobInput),
      concatLatestFrom(() => [
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(filterNullish())
      ]),
      map(([action, slideId, extended]) =>
        JobsActions.setWsiJobInputReady({
          ...action,
          slideId,
          slideInputKey: this.eadService.getInputKeys(extended.ead)[0],
          annotationInputKey: this.eadService.getInputKeys(extended.ead)[1]
        })
      )
    );
  });

  setWsiJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setWsiJobInputReady),
      concatMap(action => this.jobsPanelService.scopeIdJobsJobIdInputsInputKeyPut({
          scope_id: action.scopeId,
          job_id: action.jobId,
          input_key: action.slideInputKey,
          body: {
            id: action.slideId
          }
        }).pipe(
          // retry to load the selected slide after the token is expired
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          map(() => JobsActions.setAnnotationJobInput({ ...action })),
          catchError(error => of(JobsActions.setJobInputFailure({ error, ...action })))
        )
      )
    )
  });

  setAnnotationJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setAnnotationJobInput),
      concatMap(action => this.jobsPanelService.scopeIdJobsJobIdInputsInputKeyPut({
          scope_id: action.scopeId,
          job_id: action.jobId,
          input_key: action.annotationInputKey,
          body: {
            id: action.annotationId
          }
        }).pipe(
          // retry to load the selected slide after the token is expired
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          map(() => JobsActions.runJob({ ...action })),

          catchError(error => of(JobsActions.setJobInputFailure({ error, ...action })))
        )
      )
    )
  });

  runJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.runJob),
        concatMap(action => this.jobsPanelService.scopeIdJobsJobIdRunPut({
          scope_id: action.scopeId,
          job_id: action.jobId
        }).pipe(
          // retry to load the selected slide after the token is expired
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          map(job => JobsActions.createJobSuccess({ job })),
          catchError(error => of(JobsActions.setJobInputFailure({ error, ...action })))
        )
      )
    )
  });

  deleteJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.deleteJob),
      concatMap(action => this.jobsPanelService.scopeIdJobsJobIdDelete({
          scope_id: action.scopeId,
          job_id: action.jobId,
        }).pipe(
          // retry to load the selected slide after the token is expired
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          map(job => job.id),
          map(jobId => JobsActions.deleteJobSuccess({ jobId })),
          catchError(error => of(JobsActions.deleteJobFailure({ error })))
        )
      )
    )
  });

  prepareStopRunningJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.stopRunningJob),
      map(action => action.jobId),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())
      ),
      map(([jobId, scopeId]) => JobsActions.stopRunningJobReady({
        scopeId,
        jobId
      }))
    );
  });

  stopRunningJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.stopRunningJobReady),
      concatMap(action => this.jobsPanelService.scopeIdJobsJobIdStopPut({
          scope_id: action.scopeId,
          job_id: action.jobId
        }).pipe(
          // retry to load the selected slide after the token is expired
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          map(done => JobsActions.stopRunningJobSuccess({ done })),
          // TODO: return null?
          catchError(error => of(JobsActions.stopRunningJobFailure({ error })))
        )
      )
    )
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly jobsPanelService: JobsPanelService,
    private readonly eadService: EadService,
  ) {}

}
