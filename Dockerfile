###### Stage 1: Install dependencies and build project ######
FROM node:16-alpine AS builder

WORKDIR /app

COPY . .

RUN npm install --legacy-peer-deps
RUN npm run build --output-path=dist --configuration=production --output-hashing=all

##### Stage 2 #####
FROM nginx:1.17.1-alpine

# Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /app/dist/kidney-ui-app /usr/share/nginx/html
